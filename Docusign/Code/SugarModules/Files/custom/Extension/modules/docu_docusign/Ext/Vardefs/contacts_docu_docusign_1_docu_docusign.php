<?php
// created: 2017-05-23 09:07:29
$dictionary["docu_docusign"]["fields"]["contacts_docu_docusign_1"] = array (
  'name' => 'contacts_docu_docusign_1',
  'type' => 'link',
  'relationship' => 'contacts_docu_docusign_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_DOCU_DOCUSIGN_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_docu_docusign_1contacts_ida',
);
$dictionary["docu_docusign"]["fields"]["contacts_docu_docusign_1_name"] = array (
  'name' => 'contacts_docu_docusign_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_DOCU_DOCUSIGN_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_docu_docusign_1contacts_ida',
  'link' => 'contacts_docu_docusign_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["docu_docusign"]["fields"]["contacts_docu_docusign_1contacts_ida"] = array (
  'name' => 'contacts_docu_docusign_1contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_docu_docusign_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_DOCU_DOCUSIGN_1_FROM_DOCU_DOCUSIGN_TITLE',
);
