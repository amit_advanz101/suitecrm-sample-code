<?php 
// initialize a temp array that will hold the options we want to create
$links = array();
// add button1 to $links
$links['docusign_Module']['link1'] = array(
    // pick an image from /themes/Sugar5/images
    // and drop the file extension
    'Releases',
    // title of the link 
    'DocuSign Settings',
    // description for the link
    'DocuSign configuration options',
    // where to send the user when the link is clicked
    './index.php?module=Administration&action=docusign_settings',
);
// add link2 to $links
// this link uses labels instead of hard-coded strings
// since this is in the scope of the Administration module, 
// you need to add these labels to the 
// Administration lang files in 
// /custom/Extension/modules/Administration/Ext/Language/abc_Module.php
/*$links['abc_Module']['link2'] = array(
    // pick an image from /themes/Sugar5/images
    // and drop the file extension
    'CallReports',
    // defined in /custom/Extension/modules/Administration/Ext/Language/abc_Module.php
    'LBL_ABC_MODULE_LINK2_TITLE',
    // defined in /custom/Extension/modules/Administration/Ext/Language/abc_Module.php
    'LBL_ABC_MODULE_LINK2_DESCRIPTION',
    // where to send the user when the link is clicked
    './index.php?module=abc_Module&action=link2_action', 
);*/


// add our new admin section to the main admin_group_header array
$admin_group_header []= array(
    // The title for the group of links
    'DocuSign Settings', 
    // leave empty, it's used for something in /include/utils/layout_utils.php 
    // in the get_module_title() function
    '', 
    // set to false, it's used for something in /include/utils/layout_utils.php 
    // in the get_module_title() function
    false, 
    // the array of links that you created above
    // to be placed in this section
    $links, 
    // a description for what this section is about
    ''
);