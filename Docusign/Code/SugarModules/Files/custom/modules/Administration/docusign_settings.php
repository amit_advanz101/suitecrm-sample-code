<?php 

    $db = DBManagerFactory::getInstance();
    
    $docusignSettingSql = "SELECT * FROM docusign LIMIT 1";
    $res_docusign_setting = $db->query($docusignSettingSql);
    $docusign_setting_row = $db->fetchByAssoc($res_docusign_setting);
    
    if(!empty($docusign_setting_row)){
        $d_email = $docusign_setting_row['email'];
        $d_password = $docusign_setting_row['password'];
        $d_key = $docusign_setting_row['docusign_key'];
        $d_environment = $docusign_setting_row['environment'];
    }else{
        $d_email = $_POST['docusign_email'];
        $d_password = $_POST['docusign_password'];
        $d_key = $_POST['docusign_key'];
        $d_environment = $_POST['docusign_env'];
    }

    if($_POST['submitDocusign']){
        $docusign_email = $_POST['docusign_email'];
        $docusign_passowrd = $_POST['docusign_password'];
        $docusign_key = $_POST['docusign_key'];
        $docusign_env = $_POST['docusign_env'];
        
        if(empty($docusign_setting_row)){
            $sql = "INSERT INTO docusign (id,email,password,docusign_key,environment) VALUES (SUBSTR(CONCAT(MD5(RAND()),MD5(RAND())),1,36),'$docusign_email','$docusign_passowrd','$docusign_key','$docusign_env')";
        }else{
            $sql = "UPDATE docusign SET email='".$docusign_email."',password='".$docusign_passowrd."',docusign_key='".$docusign_key."',environment='".$docusign_env."'";
        }
        
        $db->query($sql);
        
        echo '<span style="color:green;">Settings saved successfully</span>';
    }
?>

<form method="POST" action="" enctype="multipart/form-data">
    
    <div class="buttons">
        <input title="Save" class="button primary" type="submit" name="submitDocusign" value="Save" id=""> 
        <input title="Cancel" class="button" type="button" name="button" value="Cancel" id="" onclick="window.location.href='index.php?module=Administration&action=index'"> 
    </div>
    <br />
    
    
    <div class="panel panel-default">
        <div class="panel-heading ">
            <a class="" role="button" data-toggle="collapse" aria-expanded="false">
                <div class="col-xs-10 col-sm-11 col-md-11">
                DocuSign Settings
                </div>
            </a>
        </div>
        
        <div class="panel-body panel-collapse collapse in" id="detailpanel_-1">
            <div class="tab-content">
            <!-- tab_panel_content.tpl -->
                <div class="row edit-view-row">
                    <div class="col-xs-12 col-sm-6 edit-view-row-item">
                        <div class="col-xs-12 col-sm-4 label">
                            DocuSign Email:
                            <span class="required"></span>
                        </div>

                        <div class="col-xs-12 col-sm-8 edit-view-field ">
                            <input type="email" name="docusign_email" id="" size="30" title="" value="<?php echo $d_email?>" required>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 edit-view-row-item">
                        <div class="col-xs-12 col-sm-4 label">
                            DocuSign Password:
                            <span class="required"></span>
                        </div>

                        <div class="col-xs-12 col-sm-8 edit-view-field ">
                            <input type="password" name="docusign_password" id="" size="30" title="" value="<?php echo $d_password?>" required>
                        </div>
                    </div>

                    <div class="clear"></div>
                    <div class="clear"></div>

                     <div class="col-xs-12 col-sm-6 edit-view-row-item">
                        <div class="col-xs-12 col-sm-4 label">
                            Integration Key:
                            <span class="required"></span>
                        </div>

                        <div class="col-xs-12 col-sm-8 edit-view-field">
                            <input type="text" name="docusign_key" id="" size="30" title="" value="<?php echo $d_key?>" required>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 edit-view-row-item">
                        <div class="col-xs-12 col-sm-4 label">
                            Environment:
                            <span class="required"></span>
                        </div>

                        <div class="col-xs-12 col-sm-8 edit-view-field">
                            
                            <select name="docusign_env">
                                <option value="">--Select--</option>
                                <option value="Sendbox" <?php if($d_environment=='Sendbox') echo ' selected="selected"';?>>Sendbox</option>
                                <option value="dev" <?php if($d_environment=='dev') echo ' selected="selected"';?>>Dev</option>
                                <option></option>

                            </select>
                        </div>
                    </div>
                </div>                    
            </div>
        </div>
    </div>
    
    
    
    
</form>