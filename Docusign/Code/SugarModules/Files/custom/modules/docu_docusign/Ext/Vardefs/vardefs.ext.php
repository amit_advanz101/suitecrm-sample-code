<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-05-23 10:14:30
$dictionary['docu_docusign']['fields']['document_status_c']['inline_edit']='1';
$dictionary['docu_docusign']['fields']['document_status_c']['labelValue']='Document Status';

 

 // created: 2017-05-30 12:03:41
$dictionary['docu_docusign']['fields']['docusign_email_c']['inline_edit']='1';
$dictionary['docu_docusign']['fields']['docusign_email_c']['labelValue']='Docusign Email';

 

 // created: 2017-05-23 09:53:41
$dictionary['docu_docusign']['fields']['subject_c']['inline_edit']='1';
$dictionary['docu_docusign']['fields']['subject_c']['labelValue']='Subject';

 

 // created: 2017-05-23 09:11:09
$dictionary['docu_docusign']['fields']['envelope_id_c']['inline_edit']='1';
$dictionary['docu_docusign']['fields']['envelope_id_c']['labelValue']='Envelope Id';

 

 // created: 2017-05-23 10:14:14
$dictionary['docu_docusign']['fields']['document_name_c']['inline_edit']='1';
$dictionary['docu_docusign']['fields']['document_name_c']['labelValue']='Document Name';

 

// created: 2017-05-23 09:07:29
$dictionary["docu_docusign"]["fields"]["contacts_docu_docusign_1"] = array (
  'name' => 'contacts_docu_docusign_1',
  'type' => 'link',
  'relationship' => 'contacts_docu_docusign_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_DOCU_DOCUSIGN_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_docu_docusign_1contacts_ida',
);
$dictionary["docu_docusign"]["fields"]["contacts_docu_docusign_1_name"] = array (
  'name' => 'contacts_docu_docusign_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_DOCU_DOCUSIGN_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_docu_docusign_1contacts_ida',
  'link' => 'contacts_docu_docusign_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["docu_docusign"]["fields"]["contacts_docu_docusign_1contacts_ida"] = array (
  'name' => 'contacts_docu_docusign_1contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_docu_docusign_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_DOCU_DOCUSIGN_1_FROM_DOCU_DOCUSIGN_TITLE',
);

?>