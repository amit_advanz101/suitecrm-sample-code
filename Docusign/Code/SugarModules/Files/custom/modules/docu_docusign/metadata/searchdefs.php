<?php
$module_name = 'docu_docusign';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'document_name_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_DOCUMENT_NAME',
        'width' => '10%',
        'name' => 'document_name_c',
      ),
      'subject_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_SUBJECT',
        'width' => '10%',
        'name' => 'subject_c',
      ),
    ),
    'advanced_search' => 
    array (
      0 => 'name',
      1 => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
