<?php
$module_name = 'docu_docusign';
$listViewDefs [$module_name] = 
array (
  'CONTACTS_DOCU_DOCUSIGN_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CONTACTS_DOCU_DOCUSIGN_1_FROM_CONTACTS_TITLE',
    'id' => 'CONTACTS_DOCU_DOCUSIGN_1CONTACTS_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'DOCUMENT_NAME_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_DOCUMENT_NAME',
    'width' => '10%',
     
  ),
  'SUBJECT_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_SUBJECT',
    'width' => '10%',
  ),
  'DOCUMENT_STATUS_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_DOCUMENT_STATUS',
    'width' => '10%',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
);
?>
