<?php
// created: 2017-05-25 12:50:14
$subpanel_layout['list_fields'] = array (
  'subject_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SUBJECT',
    'width' => '20%',
  ),
  'document_name_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_DOCUMENT_NAME',
    'width' => '30%',
  ),
  'document_status_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_DOCUMENT_STATUS',
    'width' => '20%',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '20%',
    'default' => true,
  ),
);