<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="custom/modules/docu_docusign/jquery.multifile.js"></script>

<?php

global $sugar_config;
$siteUrl = $sugar_config['site_url'];
$siteUrl = ''.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'';
    
?>

<style>
    #actionMenuSidebar ul li { display:none; }
    #actionMenuSidebar ul li + li { display: list-item; }         
</style>

<script type="text/javascript">
    $(function ($)
    {
        $('#docusign_file').multifile();
    });
</script>

<script>

    $(document).ready(function () {
        $('.send_docusign_doc').click(function () {
            var x = $('#docusign_file').val();
            //var filename = x.split('\\').pop();
            //var file_extension = filename.split('.');
            if (x == '') {
                alert('Please select the document to send');
                return false;
            } else {
                /*if (file_extension[file_extension.length - 1] == 'docx' || file_extension[file_extension.length - 1] == 'doc' || file_extension[file_extension.length - 1] == 'pdf') {
                 return true;
                 } else {
                 alert('This file type is not allowed to send.');
                 return false;
                 }*/



            }
            //alert('fb');return false;


            //var modelname=$("#inputmodelname").val();
            for (var i = 0; i < $("#docusign_file").get(0).files.length; ++i) {
                var file1 = $("#docusign_file").get(0).files[i].name;
                //alert($("#docusign_file").get(0).files[i].name);
                if (file1) {
                    var file_size = $("#docusign_file").get(0).files[i].size;
                    if (file_size < 2097152) {
                        var ext = file1.split('.').pop().toLowerCase();
                        if ($.inArray(ext, ['doc', 'docx', 'pdf']) === -1) {
                            alert("File types are not allowed");
                            return false;
                        }

                    } else {
                        alert("files size is too large.");
                        return false;
                    }
                } else {
                    alert("fill all fields..");
                    return false;
                }
            }

            $(".filename").each(function () {
                var filename = $(this).html();
                filenameArr = filename.split('.');
                if (filenameArr[filenameArr.length - 1] == 'doc' || filenameArr[filenameArr.length - 1] == 'docx' || filenameArr[filenameArr.length - 1] == 'pdf') {
                    return true;
                } else {
                    alert('files type are not valid');
                    return false;
                }
            });


        });

<?php
if ($_GET['envelopeId']) {
    ?>
            //alert('fgdfg');
            parent.closeIFrame();
    <?php
}
?>

        $(document).on("click", ".remove_contact", function () {
            var id = $(this).attr('id');
            var remove_id = id.split('_');
            var contact_id = remove_id[1];
            $('#checkbox_' + contact_id).prop('checked', false);
            $('.contact_row_' + contact_id).show();
            $('.added_name_' + contact_id).remove();
            $('.added_email_' + contact_id).remove();
            var recipient_entry = $('.recipient_entry').val();
            var recipient_entry_add = parseInt(recipient_entry) - parseInt(1);
            $('.recipient_entry').val(recipient_entry_add);

        });

        $(".check_contact").change(function () {
            var id = $(this).attr('id');
            var contact_id_arr = id.split('_');
            var contact_id = contact_id_arr[1];

            $('.contact_row_' + contact_id).hide();

            var recipient_entry = $('.recipient_entry').val();
            //alert(recipient_entry);
            var recipient_entry_add = parseInt(recipient_entry) + parseInt(1);
            //alert(recipient_entry_add);
            $('.recipient_' + recipient_entry).after("<div class='col-xs-12 col-sm-6 edit-view-row-item added_name_" + contact_id + "'>\n\
    <div class='col-xs-12 col-sm-4 label'>Recipients name:<span class='required'></span></div>\n\
    \n\
    <div class='col-xs-12 col-sm-8 edit-view-field'><input type='text' name='recipient_name[]' id='' size='30' title='' value='" + $('.contact_name_' + contact_id).html() + "' required></div></div>\n\
    \n\
    <div class='col-xs-12 col-sm-6 edit-view-row-item recipient_" + recipient_entry_add + " added_email_" + contact_id + "'><div class='col-xs-12 col-sm-4 label'>Recipients Email:<span class='required'></span></div><div class='col-xs-12 col-sm-8 edit-view-field'>\n\
<input type='email' name='recipient_email[]' value='" + $('.contact_email_' + contact_id).html() + "' size='30' title='' required><input type='hidden' name='recipient_id[]' value='" + contact_id + "'>&nbsp;<a href='javascript:void(0);' class='remove_contact' id='remove_" + contact_id + "'>Remove</a></div>\n\
    </div>");

            $('.recipient_entry').val(recipient_entry_add);

        });


    });

    function closeIFrame() {
        $('.iframehide').remove();
        window.location.href = '<?php echo $siteUrl?>/index.php?module=docu_docusign&action=index';
    }

</script>

<?php

$db = DBManagerFactory::getInstance();
global $current_user;

if ($_POST['recipient_name'] != '') {

    //print_r($_POST['recipient_name']);die;


    $docusignSettingSql = "SELECT * FROM docusign LIMIT 1";
    $res_docusign_setting = $db->query($docusignSettingSql);
    $docusign_setting_row = $db->fetchByAssoc($res_docusign_setting);


    include "Docusign.php";
    $docuObj = new Docusign($docusign_setting_row['email'], $docusign_setting_row['password'], $docusign_setting_row['docusign_key'], $docusign_setting_row['environment']);
    
    $return_url = ''.$siteUrl.'/index.php?module=docu_docusign&action=manage_document&record=' . $_GET['record'] . '';

    $docusignArr = $docuObj->build_envelope($_POST['recipient_name'], $_POST['recipient_email'], $_POST['email_subject'], $_FILES['docu'], $return_url);

    $documentNames = '';
    $j = 1;
    foreach ($_FILES['docu']['name'] as $docuArrNames) {
        if ($j == sizeof($_FILES['docu']['name'])) {
            continue;
        }

        $documentNames .= $docuArrNames . ',';
        $j++;
    }

    $x = 0;
    $i = 1;
    foreach ($_POST['recipient_id'] as $recp_id) {
        $docusign_bean = BeanFactory::newBean('docu_docusign');
        $docusign_bean->envelope_id_c = $docusignArr['envelopeId'];
        $docusign_bean->docusign_email_c = $_POST['recipient_email'][$x];
        $docusign_bean->document_name_c = rtrim($documentNames, ',');
        $docusign_bean->document_status_c = 'Sent';
        $docusign_bean->subject_c = $_POST['email_subject'];
        $docusign_bean->assigned_user_id = $current_user->id;
        $docusign_bean->save();

        $docusign_record_id = $docusign_bean->id;

        $docusign_contact_rel_sql = "INSERT INTO contacts_docu_docusign_1_c (id,contacts_docu_docusign_1contacts_ida,contacts_docu_docusign_1docu_docusign_idb,date_modified,deleted) VALUES (SUBSTR(CONCAT(MD5(RAND()),MD5(RAND())),1,36),'" . $recp_id . "','" . $docusign_record_id . "',NOW(),0)";

        $db->query($docusign_contact_rel_sql);

        $x++;
    }
    ?>

    <script>
        $(document).ready(function () {
            $('.iframe_docusign').html('<iframe class="iframehide" src="<?php echo $docusignArr['url']; ?>" width="1040" height="500"></iframe>');

            $('.sel_other_contacts').hide();
        });
    </script>
    <?php
}
?>


<?php
$contactsql = "SELECT first_name,last_name,email_addresses.email_address FROM contacts
LEFT JOIN email_addr_bean_rel ON contacts.id = email_addr_bean_rel.bean_id
LEFT JOIN email_addresses ON email_addr_bean_rel.email_address_id = email_addresses.id
WHERE contacts.id = '" . $_GET['record'] . "' ORDER BY email_addr_bean_rel.date_modified DESC LIMIT 1";

$res_contact = $db->query($contactsql);
$contact_row = $db->fetchByAssoc($res_contact);
if (!empty($contact_row)) {
    $recipient_name = $contact_row['first_name'] . ' ' . $contact_row['last_name'];
    $recipient_email = $contact_row['email_address'];
} else {
    $recipient_name = '';
    $recipient_email = '';
}

if($_POST['email_subject']){
    $email_subject = $_POST['email_subject'];
}else{
    $email_subject = '';
}
?>

<form method="POST" id="frm_send_docs" action="" enctype="multipart/form-data">

    <div class="buttons">
        <input title="Send Document" class="button primary send_docusign_doc" type="submit" name="submit_document" value="Send" id=""> 
        <input title="Cancel" class="button" type="button" name="button" value="Cancel" id="" onclick="window.location.href = 'index.php?module=Contacts&action=DetailView&record=<?php echo $_GET['record'] ?>'"> 
    </div>
    <br />


    <div class="panel panel-default">
        <div class="panel-heading ">
            <a class="" role="button" data-toggle="collapse" aria-expanded="false">
                <div class="col-xs-10 col-sm-11 col-md-11">
                    Document Details
                </div>
            </a>
        </div>

        <div class="panel-body panel-collapse collapse in" id="detailpanel_-1">
            <div class="tab-content">
                <!-- tab_panel_content.tpl -->
                <div class="row edit-view-row">
                    <div class="col-xs-12 col-sm-6 edit-view-row-item">
                        <div class="col-xs-12 col-sm-4 label">
                            Recipients name:
                            <span class="required"></span>
                        </div>

                        <div class="col-xs-12 col-sm-8 edit-view-field ">
                            <input type="text" name="recipient_name[]" id="" size="30" title="" value="<?php echo $recipient_name ?>" required>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 edit-view-row-item recipient_1">
                        <div class="col-xs-12 col-sm-4 label">
                            Recipients Email:
                            <span class="required"></span>
                        </div>

                        <div class="col-xs-12 col-sm-8 edit-view-field ">
                            <input type="email" name="recipient_email[]" id="" value="<?php echo $recipient_email ?>" size="30" title="" required>
                            <input type="hidden" name="recipient_id[]" value="<?php echo $_GET['record']; ?>">
                        </div>
                    </div>

                    <!--                    <a href="javascript:void(0);" onclick="add_recipient()">Add More</a>-->
                    <input type="hidden" value="1" class="recipient_entry" />

                    <div class="clear"></div>
                    <div class="clear"></div>

                    <div class="col-xs-12 col-sm-6 edit-view-row-item">
                        <div class="col-xs-12 col-sm-4 label">
                            Subject:
                            <span class="required"></span>
                        </div>

                        <div class="col-xs-12 col-sm-8 edit-view-field">
                            <input type="text" name="email_subject" id="" value="<?php echo $email_subject ?>" size="30" title="" required>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 edit-view-row-item">
                        <div class="col-xs-12 col-sm-4 label">
                            Select Document:
                            <span class="required"></span>
                        </div>

                        <div class="col-xs-12 col-sm-8 edit-view-field">
                            <input type="file" name="docu[]" id="docusign_file" multiple="" style="margin-top:12px;" />
                        </div>
                    </div>
                </div>                    
            </div>
        </div>
    </div>


    <br /><br /><br />



    <div class="panel panel-default sel_other_contacts">
        <div class="panel-heading ">
            <a class="" role="button" data-toggle="collapse" aria-expanded="false">
                <div class="col-xs-10 col-sm-11 col-md-11">
                    Select Related Contacts
                </div>
            </a>
        </div>

        <div class="panel-body panel-collapse collapse in" id="detailpanel_-1">
            <div class="tab-content">
                <!-- tab_panel_content.tpl -->
                <div class="row edit-view-row">


                    <?php
                    $contactSql = "SELECT account_id FROM accounts_contacts WHERE contact_id='" . $_GET['record'] . "'";
                    $contact_res = $db->query($contactSql);
                    $contact_account_row = $db->fetchByAssoc($contact_res);


                    $other_contact_sql = "SELECT contacts.id,first_name,last_name,email_addresses.email_address FROM accounts_contacts
                            LEFT JOIN contacts ON accounts_contacts.contact_id = contacts.id
LEFT JOIN email_addr_bean_rel ON contacts.id = email_addr_bean_rel.bean_id
LEFT JOIN email_addresses ON email_addr_bean_rel.email_address_id = email_addresses.id
WHERE accounts_contacts.account_id = '" . $contact_account_row['account_id'] . "' AND contacts.deleted=0 AND accounts_contacts.deleted=0 AND email_addr_bean_rel.deleted=0 AND contacts.id!='" . $_GET['record'] . "'"
                            . " ORDER BY email_addr_bean_rel.date_modified DESC";
                    $res_other_contact = $db->query($other_contact_sql);
                    ?>


                    <table border="0" cellpadding="0" cellspacing="0" class="list view table-responsive">
                        <thead>
                        <th>Select</th>
                        <th>Contact Name</th>
                        <th>Contact Email</th>
                        </thead>

                        <tbody>

                            <?php
                            if (!empty($res_other_contact)) {
                                while ($row_other_contact = $db->fetchByAssoc($res_other_contact)) {
                                    ?>
                                    <tr height="20" class="oddListRowS1 contact_row_<?php echo $row_other_contact['id'] ?>">
                                        <td align="left" valign="top"><input type="checkbox" class="check_contact" id="checkbox_<?php echo $row_other_contact['id'] ?>"></td>
                                        <td class="contact_name_<?php echo $row_other_contact['id'] ?>"><?php echo $row_other_contact['first_name'] . ' ' . $row_other_contact['last_name'] ?></td>
                                        <td class="contact_email_<?php echo $row_other_contact['id'] ?>"><?php echo $row_other_contact['email_address']; ?></td>
                                    </tr>

                                    <?php
                                }
                            }
                            ?>


                        </tbody>
                    </table>


                </div>                    
            </div>
        </div>
    </div>






</form>

<div class="iframe_docusign">

</div>