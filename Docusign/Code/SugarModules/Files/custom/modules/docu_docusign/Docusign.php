<?php

class Docusign {

    protected $userEmail;
    protected $userPass;
    protected $integratorKey;
    protected $apiEndPoint;

    public function __construct($user_email, $user_pass, $integrator_key, $environment) {
        $this->userEmail = $user_email;
        $this->userPass = $user_pass;
        $this->integratorKey = $integrator_key;

        if ($environment == 'live') {
            $this->apiEndPoint = '';
        } else {
            $this->apiEndPoint = 'https://demo.docusign.net/restapi/v2/';
        }
    }

    private function callAPI($url, $requestBody, $content_type) {

        $header = "<DocuSignCredentials><Username>" . $this->userEmail . "</Username><Password>" . $this->userPass . "</Password><IntegratorKey>" . $this->integratorKey . "</IntegratorKey></DocuSignCredentials>";

        $curl = curl_init($this->apiEndPoint . $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        if ($url == 'login_information') {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("X-DocuSign-Authentication: $header"));
        } else {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $requestBody);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: ' . $content_type . '',
                'Content-Length: ' . strlen($requestBody),
                "X-DocuSign-Authentication: $header")
            );
        }

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $json_response = curl_exec($curl);

        /*if (!curl_exec($curl)) {
            die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
        }*/

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($url == 'login_information') {
            if ($status != 200) {
                echo "error calling webservice, status is:" . $status . "\nerror text is --> ";
                // print_r($json_response); echo "\n";
                exit(-1);
            }
        } else {
            if ($status != 201) {
                echo "error calling webservice, status is:" . $status . "\nerror text is --> ";
                // print_r($json_response); echo "\n";
                exit(-1);
            }
        }

        $response = json_decode($json_response, true);
        return $response;
    }

    function build_envelope($recipient_name = '', $recipient_email = '', $email_subject = '', $file = array(), $return_url = '') {

        $loginresponse = $this->callAPI('login_information', '', 'application/json');
        $accountId = $loginresponse['loginAccounts'][0]['accountId'];

        $recipientsArr = array();
        $x = 0;
        $i = 1;
        $recipientstr = '';
        foreach($recipient_name as $recipient_name_arr){
            /*$recipientsArr[$x]['name'] = $recipient_name_arr;
            $recipientsArr[$x]['email'] = $recipient_email[$x];
            $recipientsArr[$x]['recipientId'] = $i;
            $x++;
            $i++;*/
            
            $recipientstr .= '{
                "email": "'.$recipient_email[$x].'",
                "name": "'.$recipient_name_arr.'",
                "recipientId": '.$i.'

              },';
            
            $x++;
            $i++;
        }
        
        $data = array(
            "status" => "created",
            "emailSubject" => $email_subject,
            "documents" => array(
                array("documentId" => "1", "name" => $file['name'])
            ),
            "recipients" => array(
                "signers" => $recipientsArr
            ),
            /*"eventNotification" => array(
                "url" => "http://138.197.17.3/index.php?module=docu_docusign&action=get_envelope_status",
                "loggingEnabled" => "true",
                "envelopeEvents" => array(
                    array(
                       "envelopeEventStatusCode" => "sent",
                        "envelopeEventStatusCode" => "delivered",
                        "envelopeEventStatusCode" => "delivered",
                        "envelopeEventStatusCode" => "completed",
                        "envelopeEventStatusCode" => "declined",
                        "envelopeEventStatusCode" => "voided",
                        "envelopeEventStatusCode" => "sent",
                        "envelopeEventStatusCode" => "sent" 
                    ),
                    
                ),
                
                "recipientEvents" => array(
                    array(
                        "recipientEventStatusCode" => "Sent",
                        "recipientEventStatusCode" => "Delivered",
                        "recipientEventStatusCode" => "Completed",
                        "recipientEventStatusCode" => "Declined",
                        "recipientEventStatusCode" => "AuthenticationFailed",
                        "recipientEventStatusCode" => "AutoResponded",
                    ),
                    
                    
                ),
            ),*/
       
           
        );
        
        //$data_string = json_encode($data);
        //echo $data_string;die;
        
        $docuStr = '';
        $y = 1;
        $j = 0;
        foreach ($file['name'] as $docuArrNames) {
            if($y==sizeof($file['name'])){
                continue;
            }
            
            $docuStr .= '{"documentId":" ' . $y . '",
            "name": "' . $docuArrNames . '",
            "documentBase64": "' . base64_encode(file_get_contents($_FILES['docu']['tmp_name'][$j])) . '",
            "FileExtension" : "'.pathinfo($docuArrNames, PATHINFO_EXTENSION).'"    

          },';
            $j++;
            $y++;
        }
        
        $data_string = '{
  "recipients": {
    "signers": [
      '.rtrim($recipientstr,',').'
    ]
  },
  "emailSubject": "'.$email_subject.'",
  "documents": [
    '.rtrim($docuStr,',').'
  ],
  "status": "created"
}';
        
        
        
        //$file_contents = file_get_contents($file['tmp_name']);

        $documentName = $file['name'];

        /*$requestBody = "\r\n"
                . "\r\n"
                . "--myboundary\r\n"
                . "Content-Type: application/json\r\n"
                . "Content-Disposition: form-data\r\n"
                . "\r\n"
                . "$data_string\r\n"
                . "--myboundary\r\n"
                . "Content-Type:application/pdf\r\n"
                . "Content-Disposition: file; filename=\"$documentName\"; documentid=1 \r\n"
                . "\r\n"
                . "$file_contents\r\n"
                . "--myboundary--\r\n"
                . "\r\n";*/

        $response = $this->callAPI("accounts/$accountId/envelopes", $data_string, 'application/json');
        $envelopeId = $response["envelopeId"];

        $data = array("returnUrl" => "$return_url");
        $requestBody2 = json_encode($data);

        $response = $this->callAPI("accounts/$accountId/envelopes/$envelopeId/views/sender", $requestBody2, 'application/json');
        $docusignArr = array();

        //$url = $response["url"];
        $docusignArr['envelopeId'] = $envelopeId;
        $docusignArr['url'] = $response["url"];
        $docusignArr['accountId'] = $accountId;

        return $docusignArr;
    }

    function get_file($envelopeId = '') {
        $loginresponse = $this->callAPI('login_information', '', 'application/json');
        $accountId = $loginresponse['loginAccounts'][0]['accountId'];

        $header = "<DocuSignCredentials><Username>" . $this->userEmail . "</Username><Password>" . $this->userPass . "</Password><IntegratorKey>" . $this->integratorKey . "</IntegratorKey></DocuSignCredentials>";

        //$envelopeId = "9f12928e-c1e0-42f0-945f-59aefde03bdd";

        $curl = curl_init($this->apiEndPoint."accounts/$accountId/envelopes/$envelopeId/documents");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "X-DocuSign-Authentication: $header")
        );

        $json_response = curl_exec($curl);

        if (!curl_exec($curl)) {
            die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
        }

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($status != 200) {
            echo "error calling webservice, status is:" . $status;
            exit(-1);
        }

        $response = json_decode($json_response, true);
//print_r($response);die;
        curl_close($curl);
        
      // echo sizeof($response["envelopeDocuments"]);die;
        
        
        # create new zip opbject
        $zip = new ZipArchive();

        # create a temp file & open it
        $tmp_file = tempnam('.','');
        $zip->open($tmp_file, ZipArchive::CREATE);
        
        $x = 1;
        foreach ($response["envelopeDocuments"] as $document) {

            if ($x == sizeof($response["envelopeDocuments"])) {
                continue;
            }

            $docUri = $document["uri"];
            $curl = curl_init($this->apiEndPoint."accounts/$accountId" . $docUri);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                "X-DocuSign-Authentication: $header")
            );

            $data = curl_exec($curl);
            
            /*if (!curl_exec($curl)) {
                die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
            }*/
            
            $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($status != 200) {
                echo "error calling webservice, status is:" . $status;
                exit(-1);
            }


            $f = file_put_contents('./upload/docusign_document/' . $document["name"] . "-" . $envelopeId . '.pdf', $data);

            $file = './upload/docusign_document/' . $document["name"] . "-" . $envelopeId . '.pdf';
            
            $download_file = file_get_contents($file);

            #add it to the zip
            $zip->addFromString(basename($file),$download_file);
            //$zip->addFile($file);
            /*$fp = fopen($file, "r");
            $myFileName = '' . $envelopeId . "-" . $document["name"] . '.pdf';
            header("Cache-Control: maxage=1");
            header("Pragma: public");
            header("Content-type: application/zip");
            header("Content-Disposition: inline; filename=" . $myFileName . "");
            header("Content-Description: PHP Generated Data");
            header("Content-Transfer-Encoding: binary");
            header('Content-Length:' . filesize($file));
            ob_clean();
            flush();
            while (!feof($fp)) {
                $buff = fread($fp, 1024);
                print $buff;
            }*/


            $x++;
        }
        
        $zip->close();
        
        
        # send the file to the browser as a download
        header('Content-disposition: attachment; filename=Documents.zip');
        header('Content-type: application/zip');
        ob_end_clean();
        readfile($tmp_file);

    }

}
