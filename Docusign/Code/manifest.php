<?php

/* * *******************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.

 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 * ****************************************************************************** */


// THIS CONTENT IS GENERATED BY MBPackage.php
$manifest = array(
    0 =>
    array(
        'acceptable_sugar_versions' =>
        array(
            0 => '',
        ),
    ),
    1 =>
    array(
        'acceptable_sugar_flavors' =>
        array(
            0 => 'CE',
            1 => 'PRO',
            2 => 'ENT',
        ),
    ),
   
    'readme' => '',
    'key' => 'docu',
    'author' => '',
    'description' => '',
    'icon' => '',
    'is_uninstallable' => true,
    'name' => 'docusign',
    'published_date' => '2017-06-06 13:25:59',
    'type' => 'module',
    'version' => 1496755559,
    'remove_tables' => 'prompt',
);


$installdefs = array(
    'id' => 'docusign',
    'beans' =>
    array(
        0 =>
        array(
            'module' => 'docu_docusign',
            'class' => 'docu_docusign',
            'path' => 'modules/docu_docusign/docu_docusign.php',
            'tab' => true,
        ),
    ),
    'layoutdefs' =>
    array(
    ),
    'pre_execute' => array(
        0 =>  '<basepath>/pre_execute.php',
    ),
    'relationships' =>
    array(
        0 =>
        array(
            'meta_data' => '<basepath>/SugarModules/relationships/relationships/contacts_docu_docusign_1MetaData.php',
        ),
    ),
    'logic_hooks' => array(
        array(
            'module' => 'docu_docusign',
            'hook' => 'process_record',
            'order' => 1,
            'description' => 'make document name clickable',
            'file' => 'custom/modules/docu_docusign/customLogicHook.php',
            'class' => 'docusign_document',
            'function' => 'document_link',
        ),
    ),
    'custom_fields' => array(
        array(
            'name' => 'document_name_c',
            'label' => 'LBL_DOCUMENT_NAME',
            'type' => 'varchar',
            'module' => 'docu_docusign',
            'help' => '',
            'comment' => '',
            'default_value' => '',
            'max_size' => 255,
            'required' => false, // true or false
            'reportable' => true, // true or false
            'audited' => false, // true or false
            'importable' => 'true', // 'true', 'false', 'required'
            'duplicate_merge' => true, // true or false
        ),
        array(
            'name' => 'document_status_c',
            'label' => 'LBL_DOCUMENT_STATUS',
            'type' => 'varchar',
            'module' => 'docu_docusign',
            'help' => '',
            'comment' => '',
            'default_value' => '',
            'max_size' => 255,
            'required' => false, // true or false
            'reportable' => true, // true or false
            'audited' => false, // true or false
            'importable' => 'true', // 'true', 'false', 'required'
            'duplicate_merge' => true, // true or false
        ),
        array(
            'name' => 'docusign_email_c',
            'label' => 'LBL_DOCUSIGN_EMAIL',
            'type' => 'varchar',
            'module' => 'docu_docusign',
            'help' => '',
            'comment' => '',
            'default_value' => '',
            'max_size' => 255,
            'required' => false, // true or false
            'reportable' => true, // true or false
            'audited' => false, // true or false
            'importable' => 'true', // 'true', 'false', 'required'
            'duplicate_merge' => true, // true or false
        ),
        array(
            'name' => 'envelope_id_c',
            'label' => 'LBL_ENVELOPE_ID',
            'type' => 'varchar',
            'module' => 'docu_docusign',
            'help' => '',
            'comment' => '',
            'default_value' => '',
            'max_size' => 255,
            'required' => false, // true or false
            'reportable' => true, // true or false
            'audited' => false, // true or false
            'importable' => 'true', // 'true', 'false', 'required'
            'duplicate_merge' => true, // true or false
        ),
        array(
            'name' => 'subject_c',
            'label' => 'LBL_SUBJECT',
            'type' => 'varchar',
            'module' => 'docu_docusign',
            'help' => '',
            'comment' => '',
            'default_value' => '',
            'max_size' => 255,
            'required' => false, // true or false
            'reportable' => true, // true or false
            'audited' => false, // true or false
            'importable' => 'true', // 'true', 'false', 'required'
            'duplicate_merge' => true, // true or false
        ),
    ),
    'image_dir' => '<basepath>/icons',
    'copy' =>
    array(
        0 =>
        array(
            'from' => '<basepath>/SugarModules/modules/docu_docusign',
            'to' => 'modules/docu_docusign',
        ),
        1 => array(
            'from' => '<basepath>/SugarModules/Files/custom/modules/docu_docusign',
            'to' => 'custom/modules/docu_docusign',
        ),
        2 => array(
            'from' => '<basepath>/SugarModules/Files/custom/modules/Administration',
            'to' => 'custom/modules/Administration',
        ),
        3 => array(
            'from' => '<basepath>/SugarModules/Files/custom/modules/Contacts/metadata/detailviewdefs.php',
            'to' => 'custom/modules/Contacts/metadata/detailviewdefs.php',
        ),
        4 => array(
            'from' => '<basepath>/get_envelope_status.php',
            'to' => 'get_envelope_status.php',
        ),
        5 =>
        array(
            'from' => '<basepath>/SugarModules/Files/custom/Extension/modules/Administration/Ext/Administration/docusign_Module.php',
            'to' => 'custom/Extension/modules/Administration/Ext/Administration/docusign_Module.php',
        ),
        6 =>
        array(
            'from' => '<basepath>/SugarModules/Files/custom/Extension/modules/Administration/Ext/Administration/docusign_Module.php',
            'to' => 'custom/Extension/modules/Administration/Ext/Administration/docusign_Module.php',
        ),
        7 =>
        array(
            'from' => '<basepath>/SugarModules/Files/custom/Extension/modules/docu_docusign/Ext/Language',
            'to' => 'custom/Extension/modules/docu_docusign/Ext/Language',
        ),
        8 =>
        array(
            'from' => '<basepath>/SugarModules/Files/custom/Extension/modules/docu_docusign/Ext/Vardefs/contacts_docu_docusign_1_docu_docusign.php',
            'to' => 'custom/Extension/modules/docu_docusign/Ext/Vardefs/contacts_docu_docusign_1_docu_docusign.php',
        ),
        9 =>
        array(
            'from' => '<basepath>/SugarModules/Files/docusign_document',
            'to' => 'upload/docusign_document',
        ),
    ),
    'language' =>
    array(
        0 =>
        array(
            'from' => '<basepath>/SugarModules/language/application/en_us.lang.php',
            'to_module' => 'application',
            'language' => 'en_us',
        ),
    ),
    'administration' => array(
        array(
            'from' => '<basepath>/SugarModules/Files/custom/Extension/modules/Administration/Ext/Administration/docusign_Module.php'
        )
    ),
);
