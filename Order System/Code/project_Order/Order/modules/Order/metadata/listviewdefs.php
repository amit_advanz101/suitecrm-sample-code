<?php
$module_name = 'nm_Order';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '7%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'STATUS' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'CASE_NO' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CASE_NO',
    'width' => '10%',
    'default' => true,
  ),
  'NM_ORDER_ACCOUNTS_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_NM_ORDER_ACCOUNTS_FROM_ACCOUNTS_TITLE',
    'id' => 'NM_ORDER_ACCOUNTSACCOUNTS_IDA',
    'width' => '15%',
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
);
?>
