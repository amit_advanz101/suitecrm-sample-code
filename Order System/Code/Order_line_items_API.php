<?php
   require_once('api_config.php');

    //function to make cURL request
    function call($method, $parameters, $url)
    {
        ob_start();
        $curl_request = curl_init();

        curl_setopt($curl_request, CURLOPT_URL, $url);
        curl_setopt($curl_request, CURLOPT_POST, 1);
        curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($curl_request, CURLOPT_HEADER, 1);
        curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

        $jsonEncodedData = json_encode($parameters);

        $post = array(
             "method" => $method,
             "input_type" => "JSON",
             "response_type" => "JSON",
             "rest_data" => $jsonEncodedData
        );

        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($curl_request);
        curl_close($curl_request);

        $result = explode("\r\n\r\n", $result, 2);
        $response = json_decode($result[1]);
        ob_end_flush();

        return $response;
    }

    //login -----------------------------------------     
	$login_parameters = array(
         "user_auth" => array(
              "user_name" => $username,
              "password" => md5($password),
              "version" => "1"
         ),
         "application_name" => "RestTest",
         "name_value_list" => array(),
    );

    $login_result = call("login", $login_parameters, $url);
    
    //get session id
    $session_id = $login_result->id;
	
	$data = json_decode(file_get_contents("php://input"), true);
		

$OrderID			=	$data['order']['OrderID'];
$description		=	$data['order']['description'];
$assigned_user_id	=	'1';
$CreatedBY			=	'1';
$modified_user_id	=	'1';
$deleted			=	'0';
$Status				=	$data['order']['Status'];
$accountEmail		=	$data['order']['email_id'];
$cod_charge			=	$data['order'][''];
$customer_id		=	$data['order']['customer_id'];
$order_type			=	$data['order']['order_type'];


//$accountEmail		=	$data['order']['accountEmail'];


/*Invoice FIELDS*/

$coupon_Discount	=	'';

$currency_id='';
$product_cost_price='';		

$tax_amount='';
$deleted='0';
$CreatedBY='1';
$assigned_user_id='1';
$modified_user_id='1';

//INVOICE MODULE FIELDS	
$Order_ID='';
$AccountID='';
//UPDATE
$InvoiceID = '';
$line_item_GROUP_ID = '';
$products_quotesID = '';

$AccountID='';
$OID='';


//Validating Order if exist than only updating
if($OrderID!=''){
	$get_entry_list_parameters = array(
		 'session' => $session_id,
		 'module_name' => 'nm_Order',
		 'query' => " nm_order.name='".$OrderID."'",
		 'order_by' => "",
		 'offset' => '0',
		 'select_fields' => array(
			  'id',
			  'name',
		 ),
		 'link_name_to_fields_array' => array(),
		 'max_results' => '',
		 'deleted' => '0',
		 'Favorites' => false,
	);
	$get_entry_list_result = call('get_entry_list', $get_entry_list_parameters, $url);
	if(count($get_entry_list_result->entry_list)>0)
		$OID=$get_entry_list_result->entry_list[0]->id;
	
/********************* START :: FOR UPDATE / CREATE INVOICE IN CRM *************************/
$Order_ID  = $OID;
	
$GROUP_Total_Amt = 0;
$GROUP_Savng = 0;
$GROUP_SubTotal = 0;
$GROUP_Total = 0;
$GROUP_shipping_amount = 0;
$ProdCount = count($data['items']);

if($ProdCount > 0 && $Order_ID != ''){
	foreach($data['items'] as $lineItem => $itemDetail){
		$product_qty		= 	$data['items'][$lineItem]['product_qty'];	
		$prodListPice		= 	$data['items'][$lineItem]['Purchase_Price'];	
		$prod_description	= 	$data['items'][$lineItem]['Drug_Code'];	
		$Generic_Name		= 	$data['items'][$lineItem]['Generic_Name']; 
		$product_name		= 	$data['items'][$lineItem]['Us_Brand_Name']; 
		$product_discount	=	$data['items'][$lineItem]['product_discount'];
		$cat_Discount		=	$data['items'][$lineItem]['cat_Discount'];
		$coupon_Discount	=	$data['items'][$lineItem]['coupon_Discount'];
		//$shipping_amount	=	$data['items'][$lineItem]['Prod_Shipping'];		
		$product_Order_Id	=	$data['items'][$lineItem]['Order_Id'];		
		$Discount_Amt		=	0;
		$GROUP_Total_Amt 	= 	$GROUP_Total_Amt + ($prodListPice*$product_qty);
		$GROUP_Savng		=	$GROUP_Savng + ($cat_Discount + $coupon_Discount + $product_discount )*$product_qty;
		$GROUP_shipping_amount = $GROUP_shipping_amount + $shipping_amount;
		
	}//die;
	
$GROUP_SubTotal		=	$GROUP_Total_Amt - $GROUP_Savng;
$GROUP_Total		=	$GROUP_SubTotal	+ $prod_shipping;

if($Order_ID !=''){
	//GETTING AOS_Line_Item_Groups ID WHERE PARENT ID = ORDER ID
	$get_entry_line_item_parameters = array(
		 'session' => $session_id,
		 'module_name' => 'AOS_Line_Item_Groups',
		 'query' => " aos_line_item_groups.parent_id = '".$Order_ID."' ",
		 'order_by' => "",
		 'offset' => '0',
		 'select_fields' => array(
			  'id',
			  'name',
		 ),
		 'link_name_to_fields_array' => array(
		 ),
		 'max_results' => '',
		 'deleted' => '0',
		 'Favorites' => false,
	);
	
	$get_entry_lineItem_result = call('get_entry_list', $get_entry_line_item_parameters, $url);

	if(count($get_entry_lineItem_result->entry_list)>0){
		$line_item_GROUP_ID = $get_entry_lineItem_result->entry_list[0]->name_value_list->id->value;				
	}	
	
	/* START ********* LINE ITEM GROUP CREATE / UPDATE *********/
	/* if($line_item_GROUP_ID != ''){	
		//LINE ITEM GROUP UPDATED
		$set_Line_Item_Entry_parameters = array(
			"session" => $session_id,
			"module_name" => "AOS_Line_Item_Groups",
			"name_value_list" => array(
				array('name' => 'id', 'value' => $line_item_GROUP_ID),
				array('name' => 'name', 'value' => 'Order Lineitem Group'),
				array('name' => 'description', 'value' => $description),
				array('name' => 'assigned_user_id','value' => $assigned_user_id),
				array('name' => 'modified_user_id','value' => $modified_user_id),
				array('name' => 'created_by', 'value' => $CreatedBY),
				array('name' => 'deleted', 'value' => $deleted),
				array('name' => 'total_amt', 'value' => $GROUP_Total_Amt),
				array('name' => 'discount_amount', 'value' => $GROUP_Savng),
				array('name' => 'subtotal_amount', 'value' => $GROUP_SubTotal),
				array('name' => 'tax_amount', 'value' => $tax_amount),
				//array('name' => 'subtotal_tax_amount', 'value' => $subtotal_tax_amount),
				array('name' => 'total_amount', 'value' => $GROUP_Total),
				array('name' => 'parent_type', 'value' => "nm_Order"),
				array('name' => 'parent_id', 'value' => $Order_ID),	
				array('name' => 'currency_id', 'value' => $currency_id),

			),
		);
		$set_Line_Item_Entry_result = call("set_entry", $set_Line_Item_Entry_parameters, $url);

	}else{
		// LINE ITEM GROUP CREATED
		$set_Line_Item_Entry_parameters = array(
			"session" => $session_id,
			"module_name" => "AOS_Line_Item_Groups",
			"name_value_list" => array(
				array('name' => 'name', 'value' => 'Order Lineitem Group'),
				array('name' => 'description', 'value' => $description),
				array('name' => 'assigned_user_id','value' => $assigned_user_id),
				array('name' => 'modified_user_id','value' => $modified_user_id),
				array('name' => 'created_by', 'value' => $CreatedBY),
				array('name' => 'deleted', 'value' => $deleted),
				array('name' => 'total_amt', 'value' => $GROUP_Total_Amt),
				array('name' => 'discount_amount', 'value' => $GROUP_Savng),
				array('name' => 'subtotal_amount', 'value' => $GROUP_SubTotal),
				array('name' => 'tax_amount', 'value' => $tax_amount),
				//array('name' => 'subtotal_tax_amount', 'value' => $subtotal_tax_amount),
				array('name' => 'total_amount', 'value' => $GROUP_Total),
				array('name' => 'parent_type', 'value' => "nm_Order"),
				array('name' => 'parent_id', 'value' => $Order_ID),	
				array('name' => 'currency_id', 'value' => $currency_id),

			),
		);
		$set_Line_Item_Entry_result = call("set_entry", $set_Line_Item_Entry_parameters, $url);
	} */
	
	/* END ********* LINE ITEM GROUP CREATE / UPDATE *********/
	
	$get_entry_prodQuote_parameters = array(
				 'session' => $session_id,
				 'module_name' => 'AOS_Products_Quotes',
				 'query' => " aos_products_quotes.parent_id = '".$Order_ID."' ",
				 'order_by' => "",
				 'offset' => '0',
				 'select_fields' => array(
					  'id',
					  'name',
				 ),
				 'link_name_to_fields_array' => array(
				 ),
				 'max_results' => '',
				 'deleted' => '0',
				 'Favorites' => false,
			);

			$get_entry_prodQuote_result = call('get_entry_list', $get_entry_prodQuote_parameters, $url);
			
			/*DELETE :: BEFORE ADD/UPDATE DRUG ITEM,DELETE ALL DRUG ITEM */
			$drugItemCount = count($get_entry_prodQuote_result->entry_list);
			if($drugItemCount > 0){
				for($i = 0; $i < $drugItemCount; $i++){
					$products_quotesID = $get_entry_prodQuote_result->entry_list[$i]->name_value_list->id->value;
					if($products_quotesID != ''){
						$set_Prod_Quote_entry_parameters = array(
						"session" => $session_id,
						"module_name" => "AOS_Products_Quotes",
						"name_value_list" => array(
							array('name' => 'id', 'value' => $products_quotesID),
							array('name' => 'deleted', 'value' => '1'),
						),
					);
					$set_Prod_Quote_entry_result = call("set_entry", $set_Prod_Quote_entry_parameters, $url);
					}
				}
			}
//echo"<pre>"; 	print_r($get_entry_prodQuote_result);	die;	
	foreach($data['items'] as $lineItem => $itemDetail){
		$product_qty		= 	$data['items'][$lineItem]['product_qty'];	
		$prodListPice		= 	$data['items'][$lineItem]['Purchase_Price'];	
		$prod_description	= 	$data['items'][$lineItem]['Drug_Code'];	
		$Generic_Name		= 	$data['items'][$lineItem]['Generic_Name']; 
		$product_name		= 	$data['items'][$lineItem]['Us_Brand_Name']; 
		$product_discount	=	$data['items'][$lineItem]['product_discount'];
		$cat_Discount		=	$data['items'][$lineItem]['cat_Discount'];
		$coupon_Discount	=	$data['items'][$lineItem]['coupon_Discount'];
		//$shipping_amount	=	$data['items'][$lineItem]['Prod_Shipping'];		
		$product_Order_Id	=	$data['items'][$lineItem]['Order_Id'];		
		$item_status	=	$data['items'][$lineItem]['item_status'];		
		
		/* $GROUP_Total_Amt 	= 	$GROUP_Total_Amt + ($prodListPice*$product_qty);
		$product_unit_price = 	$prodListPice - ($product_discount + $cat_Discount + $coupon_Discount);
		$Savings		=	($product_discount + $cat_Discount + $coupon_Discount);
		$product_total_price= 	$product_unit_price*$product_qty ; */
		
		/* START ********* PRODUCT QUOTE CREATE / UPDATE *********/
		
		if($product_name != ''){
				$set_Prod_Quote_entry_parameters = array(
					"session" => $session_id,
					"module_name" => "AOS_Products_Quotes",
					"name_value_list" => array(
						array('name' => 'name', 'value' => $product_name),
						array('name' => 'description', 'value' => $description),
						array('name' => 'assigned_user_id','value' => $assigned_user_id),
						array('name' => 'modified_user_id','value' => $modified_user_id),
						array('name' => 'created_by', 'value' => $CreatedBY),
						array('name' => 'deleted', 'value' => $deleted),
						array('name' => 'currency_id', 'value' => $currency_id),
						array('name' => 'part_number', 'value' => $prod_description),
						array('name' => 'item_description', 'value' => $Generic_Name),
						array('name' => 'number', 'value' => '1'),
						array('name' => 'product_qty', 'value' => $product_qty),
						array('name' => 'product_list_price', 'value' => $prodListPice),
						array('name' => 'product_discount', 'value' => $product_discount),
						array('name' => 'product_discount_amount', 'value' => $product_discount),
						//array('name' => 'discount', 'value' => $discount),
						//array('name' => 'product_unit_price', 'value' => $product_unit_price),
						array('name' => 'product_total_price', 'value' => $prodListPice),
						array('name' => 'parent_type', 'value' => "nm_Order"),
						array('name' => 'parent_id', 'value' => $Order_ID),
						array('name' => 'item_status', 'value' => $item_status),
						//array('name' => 'group_id', 'value' => $set_Line_Item_Entry_result->id),
					),
				);
				$set_Prod_Quote_entry_result = call("set_entry", $set_Prod_Quote_entry_parameters, $url);
				echo "<br>Success: DrugItem ".$product_name." created successfully !! ";
				//}
				/* START ********* PRODUCT QUOTE CREATE / UPDATE *********/
			}else{
				Echo "<br>Failure: Product Name field is required, can't empty !!";
			}
		}
	
	}else{
		Echo "<br>Failure: Order ID ".$OrderID." Not Found in CRM.";
	}	
}else{
		Echo "<br>Failure: product_qty field is required, can't empty or less than 1.";
	}
	
}else{
	echo "<br>Failure: Order ID can't Empty";
}	
?>