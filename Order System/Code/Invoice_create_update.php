 <?php
 error_reporting(E_ALL);
	require_once('api_config.php');

    //function to make cURL request
    function call($method, $parameters, $url)
    {
        ob_start();
        $curl_request = curl_init();

        curl_setopt($curl_request, CURLOPT_URL, $url);
        curl_setopt($curl_request, CURLOPT_POST, 1);
        curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($curl_request, CURLOPT_HEADER, 1);
        curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

        $jsonEncodedData = json_encode($parameters);

        $post = array(
             "method" => $method,
             "input_type" => "JSON",
             "response_type" => "JSON",
             "rest_data" => $jsonEncodedData
        );

        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($curl_request);
        curl_close($curl_request);

        $result = explode("\r\n\r\n", $result, 2);
        $response = json_decode($result[1]);
        ob_end_flush();

        return $response;
    }

    //login -----------------------------------------     
	$login_parameters = array(
         "user_auth" => array(
              "user_name" => $username,
              "password" => md5($password),
              "version" => "1"
         ),
         "application_name" => "RestTest",
         "name_value_list" => array(),
    );

    $login_result = call("login", $login_parameters, $url);
    
    //get session id
    $session_id = $login_result->id;
	
	$data = json_decode(file_get_contents("php://input"), true);
	
	//$OrderID='NMS160817892907N'; //Netmed generated OrderID
	
	//$accountEmail= 'shukla.nitin2907@gmail.com';//RightNow we are using AccountName in get_entry_list 
	
	
	//INVOICE INPUT
 	/* $name='INVOICE CREATE API TEST';
	$description='Hi , This is only test invoice CREATE API by Mr. Nitin';
	$product_unit_price='90';				//product_unit_price
	$product_qty = 1;
	$product_discount='10.00';		//Prod_Discount i.e. discount % on perticuler product 
	$discount='Percentage';	//cat_Discount => discount TYPE i.e. % OR $
	$coupon_Discount='';
	$GROUP_SubTotal='1083.45';	//Invoice Amount excluding extra charge like COD/Shipping/Tax etc.
	$GROUP_Savng='78.77';						//Savings (Total Saving)
	$GROUP_Total_Amt='1125.24';	//Amount after apply discount excluding All Tax
	$Discount_Amt='78.77';		//Amount that deduct after appy discount
	$shipping_amount='';		//Shipping charges
	$tax_amount='36.98';
	$shipping_tax='';		//Taxes % on Shipping charges if applicable i.e. 5% or 10% etc. 
	$shipping_tax_amt='';	//TAX amount that will be added in subtotal amount
	//$GROUP_TotalVAT='36.98';
	$voucher_Amt='';
	$Status='Paid';
	$InvoiceRelated_ORDERID= 'NMT1609081485N'; 	//ORDER_ID TO WHOM WE GENERATING INVOICE
	$product_name= 'ITEM-101-Test'; 			//PRODUCT ID THAT HAS BEEN PURCHASED BY USER
	$spl_Notes = ''; 
	$InvoiceUrl = 'http://api.netmeds.com/nmsapi/nms/v9/Invoice/download?order_id=NMS1610281296759W&Login_Name=v4venkatesan88@gmail.com&token=bdf270d2020642c32d007f9898e5c4ce'; */
	/*INVOICE FIELDS*/
	
	/* $ArrayLineItem['Line_items'][]= array(
						"product_qty" => 1,
						"Purchase_Price" => 330.00,
						"Drug_Code" => '59600573',
						"Generic_Name" => 'HIMALA',
						"Us_Brand_Name" => 'HIMALAYA BABY MASSAGE OIL 500ML',
						"product_discount" => '',
						"product_vat" => '',
						"cat_Discount" => '',
						"coupon_Discount" => '',
						"discount" => 'Percentage',
						"Order_Id" => 'NMT1609081485N'
					);
	$ArrayLineItem['Line_items'][]= array(
						"product_qty" => 1,
						"Purchase_Price" => 628.57,
						"Drug_Code" => '6226766503',
						"Generic_Name" => 'PROCTE',
						"Us_Brand_Name" => 'PAMPERS ACTIVE BABY (S) 46',
						"product_discount" => '',
						"product_vat" => '',
						"cat_Discount" => '',
						"coupon_Discount" => '',
						"discount" => 'Percentage',
						"Order_Id" => 'NMT1609081485N'

					);
	$ArrayLineItem['Line_items'][]= array(
						"product_qty" => 1,
						"Purchase_Price" => 166.67,
						"Drug_Code" => 'B42600032',
						"Generic_Name" => 'HIMALA',
						"Us_Brand_Name" => 'HIMALAYA SOOTHING BABY WIPES FOR HIMALA SENSITIVE SKIN EXTRA SOFT',
						"product_discount" => '',
						"product_vat" => '5',
						"cat_Discount" => '',
						"coupon_Discount" => '',
						"discount" => 'Percentage',
						"Order_Id" => 'NMT1609081485N'

					);  */
/* 	$data = array(
			
			  "invoice" => array(
 					"name" => $name,
					"description" => $description,
					//"coupon_Discount" => $coupon_Discount,
					"GROUP_Total_Amt" => $GROUP_Total_Amt,
					"GROUP_Savng" => $GROUP_Savng,
					"GROUP_SubTotal" => $GROUP_SubTotal,
					//"GROUP_TotalVAT" => $GROUP_TotalVAT,
					"shipping_amount" => $shipping_amount,
					"tax_amount" => $tax_amount,
					"shipping_tax" => $shipping_tax,
					//"shipping_tax_amt" => $shipping_tax_amt,
					"voucher_Amt" => $voucher_Amt,
					"Status" => $Status,
					"accountEmail" => $accountEmail,
					"InvoiceRelated_ORDERID" => $InvoiceRelated_ORDERID, 
					"Line_items" => $ArrayLineItem['Line_items']
				)
			);   */ 
$assigned_user_id	=	'1';
$CreatedBY			=	'1';
$modified_user_id	=	'1';
$deleted			=	'0';
//echo "<pre>"; print_r($data);


/*Invoice FIELDS*/

$coupon_Discount	=	'';
$tax_amount			=	$data['invoice']['tax_amount'];
$name				=	$data['invoice']['name'];
$invoice_id				=	$data['invoice']['invoice_id'];
$patient_name		=	$data['invoice']['patient_name'];
$doctor_name		=	$data['invoice']['doctor_name'];
$description		=	$data['invoice']['description'];
$Status				=	$data['invoice']['Status'];
$GROUP_Total_Amt	=	$data['invoice']['GROUP_Total_Amt'];		
$GROUP_Savng		=	$data['invoice']['GROUP_Savng'];		
$GROUP_SubTotal		=	$data['invoice']['GROUP_SubTotal'];		
//$GROUP_TotalVAT		=	$data['invoice']['GROUP_TotalVAT'];		
$shipping_amount	=	$data['invoice']['shipping_amount'];		
$Email_Id			=	$data['invoice']['accountEmail'];	
$InvoiceRelated_ORDERID=$data['invoice']['InvoiceRelated_ORDERID'];
$InvoiceLink       =    $data['invoice']['InvoiceUrl'];

$currency_id='';
$Brand_Name='';
$product_cost_price='';		
$product_list_price='';					

$spl_Notes='';
$deleted='0';
$CreatedBY='1';
$assigned_user_id='1';
$modified_user_id='1';

//INVOICE MODULE FIELDS	
$Order_ID='';
$AccountID='';
$account_name = '';
$billing_address_street = '';
$billing_address_city = '';
$billing_address_state = '';
$billing_address_postalcode = '';
$billing_address_country = '';
$shipping_address_street = '';
$shipping_address_city = '';
$shipping_address_state = '';
$shipping_address_postalcode = '';
$shipping_address_country = '';

//UPDATE
$InvoiceID = '';
$line_item_GROUP_ID = '';
$products_quotesID = '';


$OID='';

/********************* START :: FOR UPDATE / CREATE INVOICE IN CRM *************************/
	
//GET ACCOUNT ADDRESS etc.
$AccountID='';
if($Email_Id!=''){
	$email_UPC=strtoupper($Email_Id);
	//GETTING email_addr_bean_rel  Entry
	
	$get_entry_list_parameters = array(
		 'session' => $session_id,
		 'module_name' => 'Accounts',
		 'query' => " accounts.id IN (SELECT bean_id FROM email_addr_bean_rel eabr JOIN email_addresses ea ON (eabr.email_address_id = ea.id) WHERE bean_module = 'Accounts' AND ea.email_address_caps LIKE '%".$email_UPC."' AND eabr.deleted=0) ",
		 'order_by' => "",
		 'offset' => '0',
		 'select_fields' => array(
			  'id',
			  'name',
			  'billing_address_street',
			  'billing_address_city',
			  'billing_address_state',
			  'billing_address_postalcode',
			  'billing_address_country',
			  'shipping_address_street',
			  'shipping_address_city',
			  'shipping_address_state',
			  'shipping_address_postalcode',
			  'shipping_address_country',
		 ),
		 'link_name_to_fields_array' => array(array('name' => 'email_addresses', 'value' => array('id', 'email_address', 'opt_out', 'primary_address'))),
			
		 'max_results' => '',
		 'deleted' => '0',
		 'Favorites' => false,
	);
	$get_entry_list_result = call('get_entry_list', $get_entry_list_parameters, $url);
	if(count($get_entry_list_result->entry_list)>0){
		$AccountID=$get_entry_list_result->entry_list[0]->id;
		$account_name=$get_entry_list_result->entry_list[0]->name_value_list->name->value;
		$billing_address_street=$get_entry_list_result->entry_list[0]->name_value_list->billing_address_street->value;
		$billing_address_city=$get_entry_list_result->entry_list[0]->name_value_list->billing_address_city->value;
		$billing_address_state=$get_entry_list_result->entry_list[0]->name_value_list->billing_address_state->value;
		$billing_address_postalcode=$get_entry_list_result->entry_list[0]->name_value_list->billing_address_postalcode->value;
		$billing_address_country=$get_entry_list_result->entry_list[0]->name_value_list->billing_address_country->value;
		$shipping_address_street=$get_entry_list_result->entry_list[0]->name_value_list->shipping_address_street->value;
		$shipping_address_city=$get_entry_list_result->entry_list[0]->name_value_list->shipping_address_city->value;
		$shipping_address_state=$get_entry_list_result->entry_list[0]->name_value_list->shipping_address_state->value;
		$shipping_address_postalcode=$get_entry_list_result->entry_list[0]->name_value_list->shipping_address_postalcode->value;
		$shipping_address_country=$get_entry_list_result->entry_list[0]->name_value_list->shipping_address_country->value;
	}
}else{
	echo "<br>Failure: Email ID is Primary key, can't Empty.";
}
		
//GET ORDER DATE
if($InvoiceRelated_ORDERID !=''){
	$get_entry_list_order_parameters = array(
		 'session' => $session_id,
		 'module_name' => 'nm_Order',
		 'query' => " nm_order.name='".$InvoiceRelated_ORDERID."'",
		 'order_by' => "",
		 'offset' => '0',
		 'select_fields' => array(
			  'id',
			  'name',
			  'date_entered',
		 ),
		 
		 'link_name_to_fields_array' => array(
		 ),
		 'max_results' => '',
		 'deleted' => '0',
		 'Favorites' => false,
	);

	$get_entry_list_order_result = call('get_entry_list', $get_entry_list_order_parameters, $url);

	if(count($get_entry_list_order_result->entry_list)>0){
		$OrderDate=$get_entry_list_order_result->entry_list[0]->name_value_list->date_entered->value;
		$Order_ID=$get_entry_list_order_result->entry_list[0]->name_value_list->id->value;
	}
}else{
	echo "<br>Failure: Order ID is required to generate Invoice";
}
//&& ($name !='' &&  $product_name !=  '')
$ProdCount = count($data['invoice']['Line_items']);
if($ProdCount > 0){
if(($Order_ID !='' &&  $Email_Id !=  '') ){
	$get_entry_invoices_parameters = array(
		 'session' => $session_id,
		 'module_name' => 'AOS_Invoices',
		 'query' => " aos_invoices.name Like '".$InvoiceRelated_ORDERID."%' ",
		 'order_by' => "",
		 'offset' => '0',
		 'select_fields' => array(
			  'id',
			  'name',
		 ),
		 'link_name_to_fields_array' => array(
		 ),
		 'max_results' => '',
		 'deleted' => '0',
		 'Favorites' => false,
	);

	$get_entry_invoicest_result = call('get_entry_list', $get_entry_invoices_parameters, $url);

	if(count($get_entry_invoicest_result->entry_list)>0){
		$InvoiceNAME = $get_entry_invoicest_result->entry_list[0]->name_value_list->name->value;
		$InvoiceID = $get_entry_invoicest_result->entry_list[0]->name_value_list->id->value;
	}
//echo "<pre>"; print_r($get_entry_invoicest_result);

if($InvoiceID != '' ){
	//GETTING AOS_Line_Item_Groups ID WHERE PARENT ID = INVOICE ID
	$get_entry_line_item_parameters = array(
		 'session' => $session_id,
		 'module_name' => 'AOS_Line_Item_Groups',
		 'query' => " aos_line_item_groups.parent_id = '".$InvoiceID."' and parent_type = 'AOS_Invoices' ",
		 'order_by' => "",
		 'offset' => '0',
		 'select_fields' => array(
			  'id',
			  'name',
		 ),
		 'link_name_to_fields_array' => array(
		 ),
		 'max_results' => '',
		 'deleted' => '0',
		 'Favorites' => false,
	);

	$get_entry_lineItem_result = call('get_entry_list', $get_entry_line_item_parameters, $url);

	if(count($get_entry_lineItem_result->entry_list)>0){
		$line_item_GROUP_ID = $get_entry_lineItem_result->entry_list[0]->name_value_list->id->value;				

	}	
	//GETTING aos_products_quotes ID WHERE PARENT ID = INVOICE ID
	$get_entry_prodQuote_parameters = array(
		 'session' => $session_id,
		 'module_name' => 'AOS_Products_Quotes',
		 'query' => " aos_products_quotes.parent_id = '".$InvoiceID."' and parent_type = 'AOS_Invoices' ",
		 'order_by' => "",
		 'offset' => '0',
		 'select_fields' => array(
			  'id',
			  'name',
		 ),
		 'link_name_to_fields_array' => array(
		 ),
		 'max_results' => '',
		 'deleted' => '0',
		 'Favorites' => false,
	);

	$get_entry_prodQuote_result = call('get_entry_list', $get_entry_prodQuote_parameters, $url);

	if(count($get_entry_prodQuote_result->entry_list)>0){
		$products_quotesID = $get_entry_prodQuote_result->entry_list[0]->name_value_list->id->value;
	}	
}

$discount = 'Amount';
$GROUP_InvoiveTotal = 0;
$GROUP_shipping_amount = 0;
$ProdCount = count($data['invoice']['Line_items']);

$shipping_amount = 0;
$shipping_tax_amt = 0;$shipping_tax = 0;	
$GROUP_InvoiveTotal = $GROUP_SubTotal;
$GROUP_Total = $GROUP_SubTotal;
$GROUP_SubTotal = $GROUP_Total_Amt - $GROUP_Savng;
/* if($tax_amount != ''){
	$GROUP_InvoiveTotal = $GROUP_InvoiveTotal + (($GROUP_InvoiveTotal*$tax_amount)/100);
} */
if($InvoiceID == ''){
/**************** CREATE INVOICE *****************/
$set_entry_parameters = array(
	"session" => $session_id,
	"module_name" => "AOS_Invoices",
	"name_value_list" => array(
		array('name' => 'name', 'value' => $invoice_id),
		array('name' => 'invoice_id', 'value' => $name),
		array('name' => 'description', 'value' => $description),
		array('name' => 'total_amt', 'value' => $GROUP_Total_Amt),
		array('name' => 'discount_amount', 'value' => '-'.$GROUP_Savng),
		array('name' => 'subtotal_amount', 'value' => $GROUP_SubTotal),
		array('name' => 'shipping_amount', 'value' => $shipping_amount),
		array('name' => 'shipping_tax_amt', 'value' => $shipping_tax_amt),
		array('name' => 'shipping_tax', 'value' => $shipping_tax),
		array('name' => 'tax_amount', 'value' => $tax_amount),
		array('name' => 'cashback_percent', 'value' => $cashback_percent),
		array('name' => 'coupon_w_dis', 'value' => $voucher_Amt),
		array('name' => 'total_amount', 'value' => $GROUP_InvoiveTotal),
		array('name' => 'shipping_address_street', 'value' => $shipping_address_street),
		array('name' => 'shipping_address_city', 'value' => $shipping_address_city),
		array('name' => 'shipping_address_state', 'value' => $shipping_address_state),
		array('name' => 'shipping_address_postalcode', 'value' => $shipping_address_postalcode),
		array('name' => 'shipping_address_country', 'value' => $shipping_address_country),
		array('name' => 'billing_address_street', 'value' => $billing_address_street),
		array('name' => 'billing_address_city', 'value' => $billing_address_city),
		array('name' => 'billing_address_state', 'value' => $billing_address_state),
		array('name' => 'billing_address_country', 'value' => $billing_address_country),
		array('name' => 'billing_address_postalcode', 'value' => $billing_address_postalcode),
		array('name' => 'currency_id', 'value' => $currency_id),
		array('name' => 'order_date', 'value' => $OrderDate),
		array('name' => 'assigned_user_id','value' => $assigned_user_id),
		array('name' => 'modified_user_id','value' => $modified_user_id),
		array('name' => 'created_by', 'value' => $CreatedBY),
		array('name' => 'deleted', 'value' => $deleted),
		array('name' => 'doctor_name', 'value' => $doctor_name),
		array('name' => 'patient_name', 'value' => $patient_name),
		array('name' => 'status', 'value' => $Status),
		array('name' => 'billing_account_id', 'value' => $AccountID),
		array('name' => 'billing_account_name', 'value' => $account_name),
		array('name' => 'invoice_link', 'value' => $InvoiceLink),
	),
);
$set_entry_result = call("set_entry", $set_entry_parameters, $url);
	// LINE ITEM GROUP CREATED
	$set_Line_Item_Entry_parameters = array(
		"session" => $session_id,
		"module_name" => "AOS_Line_Item_Groups",
		"name_value_list" => array(
			array('name' => 'name', 'value' => 'Order Lineitem Group'),
			array('name' => 'description', 'value' => $description),
			array('name' => 'assigned_user_id','value' => $assigned_user_id),
			array('name' => 'modified_user_id','value' => $modified_user_id),
			array('name' => 'created_by', 'value' => $CreatedBY),
			array('name' => 'deleted', 'value' => $deleted),
			array('name' => 'total_amt', 'value' => $GROUP_Total_Amt),
			array('name' => 'discount_amount', 'value' => $GROUP_Savng),
			array('name' => 'subtotal_amount', 'value' => $GROUP_SubTotal),
			array('name' => 'tax_amount', 'value' => $tax_amount),
			//array('name' => 'subtotal_tax_amount', 'value' => $subtotal_tax_amount),
			array('name' => 'total_amount', 'value' => $GROUP_Total),
			array('name' => 'parent_type', 'value' => "AOS_Invoices"),
			array('name' => 'parent_id', 'value' => $set_entry_result->id),	
			array('name' => 'currency_id', 'value' => $currency_id),
		),
	);
	$set_Line_Item_Entry_result = call("set_entry", $set_Line_Item_Entry_parameters, $url);			
	// RELATION INVOICE & ORDER 
	if($Order_ID != ''){
		$set_relationship_parameters = array(
			'session' => $session_id,
			'module_name' => 'AOS_Invoices',
			'module_id' => $set_entry_result->id,
			'link_field_name' => 'nm_order_aos_invoices_1nm_order_ida',
			'related_ids' => array(
				$Order_ID,
			),
			'name_value_list' => array(
				
			),
			'delete'=> 0,
		);

		$set_relationship_result = call("set_relationship", $set_relationship_parameters, $url);
	}
	Echo "<br>Success: INVOICE has been generated successfully";

}else{
$set_entry_parameters = array(
	"session" => $session_id,
	"module_name" => "AOS_Invoices",
	"name_value_list" => array(
		array('name' => 'id', 'value' => $InvoiceID),
		array('name' => 'name', 'value' => $InvoiceRelated_ORDERID.' | '.$name),
		array('name' => 'description', 'value' => $description),
		array('name' => 'total_amt', 'value' => $GROUP_Total_Amt),
		array('name' => 'discount_amount', 'value' => '-'.$GROUP_Savng),
		array('name' => 'subtotal_amount', 'value' => $GROUP_SubTotal),
		array('name' => 'shipping_amount', 'value' => $shipping_amount),
		array('name' => 'shipping_tax_amt', 'value' => $shipping_tax_amt),
		array('name' => 'shipping_tax', 'value' => $shipping_tax),
		array('name' => 'tax_amount', 'value' => $tax_amount),
		array('name' => 'cashback_percent', 'value' => $cashback_percent),
		array('name' => 'coupon_w_dis', 'value' => $voucher_Amt),
		array('name' => 'total_amount', 'value' => $GROUP_InvoiveTotal),
		array('name' => 'shipping_address_street', 'value' => $shipping_address_street),
		array('name' => 'shipping_address_city', 'value' => $shipping_address_city),
		array('name' => 'shipping_address_state', 'value' => $shipping_address_state),
		array('name' => 'shipping_address_postalcode', 'value' => $shipping_address_postalcode),
		array('name' => 'shipping_address_country', 'value' => $shipping_address_country),
		array('name' => 'billing_address_street', 'value' => $billing_address_street),
		array('name' => 'billing_address_city', 'value' => $billing_address_city),
		array('name' => 'billing_address_state', 'value' => $billing_address_state),
		array('name' => 'billing_address_country', 'value' => $billing_address_country),
		array('name' => 'billing_address_postalcode', 'value' => $billing_address_postalcode),
		array('name' => 'currency_id', 'value' => $currency_id),
		array('name' => 'order_date', 'value' => $OrderDate),
		array('name' => 'assigned_user_id','value' => $assigned_user_id),
		array('name' => 'modified_user_id','value' => $modified_user_id),
		array('name' => 'created_by', 'value' => $CreatedBY),
		array('name' => 'deleted', 'value' => $deleted),
		array('name' => 'status', 'value' => $Status),
		array('name' => 'doctor_name', 'value' => $doctor_name),
		array('name' => 'patient_name', 'value' => $patient_name),
		array('name' => 'billing_account_id', 'value' => $AccountID),
		array('name' => 'billing_account_name', 'value' => $account_name),
		array('name' => 'invoice_link', 'value' => $InvoiceLink),
		
	),
);
$set_entry_result = call("set_entry", $set_entry_parameters, $url);
	// LINE ITEM GROUP CREATED
	$set_Line_Item_Entry_parameters = array(
		"session" => $session_id,
		"module_name" => "AOS_Line_Item_Groups",
		"name_value_list" => array(
			array('name' => 'id', 'value' => $line_item_GROUP_ID),
			array('name' => 'name', 'value' => 'Order Lineitem Group'),
			array('name' => 'description', 'value' => $description),
			array('name' => 'assigned_user_id','value' => $assigned_user_id),
			array('name' => 'modified_user_id','value' => $modified_user_id),
			array('name' => 'created_by', 'value' => $CreatedBY),
			array('name' => 'deleted', 'value' => $deleted),
			array('name' => 'total_amt', 'value' => $GROUP_Total_Amt),
			array('name' => 'discount_amount', 'value' => $GROUP_Savng),
			array('name' => 'subtotal_amount', 'value' => $GROUP_SubTotal),
			array('name' => 'tax_amount', 'value' => $tax_amount),
			//array('name' => 'subtotal_tax_amount', 'value' => $subtotal_tax_amount),
			array('name' => 'total_amount', 'value' => $GROUP_Total),
			array('name' => 'parent_type', 'value' => "AOS_Invoices"),
			array('name' => 'parent_id', 'value' => $set_entry_result->id),	
			array('name' => 'currency_id', 'value' => $currency_id),
		),
	);
	$set_Line_Item_Entry_result = call("set_entry", $set_Line_Item_Entry_parameters, $url);			
	Echo "<br>Success: INVOICE has been updated successfully";
}
foreach($data['invoice']['Line_items'] as $lineItem => $itemDetail){
	$product_vat = '0.0';
	$product_qty		= 	$data['invoice']['Line_items'][$lineItem]['product_qty'];	
	$prodListPice		= 	$data['invoice']['Line_items'][$lineItem]['Purchase_Price'];	
	$prod_description	= 	$data['invoice']['Line_items'][$lineItem]['Drug_Code'];	
	$Generic_Name		= 	$data['invoice']['Line_items'][$lineItem]['Generic_Name']; 
	$product_name		= 	$data['invoice']['Line_items'][$lineItem]['Us_Brand_Name']; 
	$product_discount	=	$data['invoice']['Line_items'][$lineItem]['product_discount'];
	$product_vat		=	$data['invoice']['Line_items'][$lineItem]['product_vat'];
	$cat_Discount		=	$data['invoice']['Line_items'][$lineItem]['cat_Discount'];
	$coupon_Discount	=	$data['invoice']['Line_items'][$lineItem]['coupon_Discount'];
	$shipping_amount	=	$data['invoice']['Line_items'][$lineItem]['Prod_Shipping'];		
	$product_Order_Id	=	$data['invoice']['Line_items'][$lineItem]['Order_Id'];		
	
	$product_unit_price = 	$prodListPice;
	$product_total_price = 	$prodListPice;
	//$product_unit_price = 	$prodListPice - $product_discount;
	//$product_unit_price = 	$prodListPice - ($product_discount + $cat_Discount + $coupon_Discount);
	$Savings		=	($product_discount + $cat_Discount + $coupon_Discount);
	//$product_total_price= 	$product_unit_price*$product_qty ;
	//$Savings			= 	$product_discount * $product_qty;
	
	if($product_name != ''){
		$get_entry_prodQuote_parameters = array(
			 'session' => $session_id,
			 'module_name' => 'AOS_Products_Quotes',
			 'query' => " aos_products_quotes.parent_id = '".$set_entry_result->id."' AND aos_products_quotes.name = '".$product_name."' ",
			 'order_by' => "",
			 'offset' => '0',
			 'select_fields' => array(
				  'id',
				  'name',
			 ),
			 'link_name_to_fields_array' => array(
			 ),
			 'max_results' => '',
			 'deleted' => '0',
			 'Favorites' => false,
		);

		$get_entry_prodQuote_result = call('get_entry_list', $get_entry_prodQuote_parameters, $url);

		if(count($get_entry_prodQuote_result->entry_list)>0){
			$products_quotesID = $get_entry_prodQuote_result->entry_list[0]->name_value_list->id->value;
		}	
		/* START ********* PRODUCT QUOTE CREATE / UPDATE *********/
		if($products_quotesID != ''){
			$set_Prod_Quote_entry_parameters = array(
			"session" => $session_id,
			"module_name" => "AOS_Products_Quotes",
			"name_value_list" => array(
				array('name' => 'id', 'value' => $products_quotesID),
				array('name' => 'name', 'value' => $product_name),
				array('name' => 'description', 'value' => $description),
				array('name' => 'assigned_user_id','value' => $assigned_user_id),
				array('name' => 'modified_user_id','value' => $modified_user_id),
				array('name' => 'created_by', 'value' => $CreatedBY),
				array('name' => 'deleted', 'value' => $deleted),
				array('name' => 'currency_id', 'value' => $currency_id),
				array('name' => 'part_number', 'value' => $prod_description),
				array('name' => 'item_description', 'value' => $Generic_Name),
				array('name' => 'number', 'value' => '1'),
				array('name' => 'product_qty', 'value' => $product_qty),
				array('name' => 'product_list_price', 'value' => $prodListPice),
				//array('name' => 'product_discount', 'value' => $product_discount),
				array('name' => 'product_discount', 'value' => $Savings),
				array('name' => 'vat', 'value' => $product_vat),
				array('name' => 'product_discount_amount', 'value' => $Savings),
				array('name' => 'discount', 'value' => $discount),
				array('name' => 'product_unit_price', 'value' => $product_unit_price),
				array('name' => 'product_total_price', 'value' => $product_total_price),
				array('name' => 'parent_type', 'value' => "AOS_Invoices"),
				array('name' => 'parent_id', 'value' => $set_entry_result->id),
				array('name' => 'product_id', 'value' => 'NULL'),
				array('name' => 'group_id', 'value' => $set_Line_Item_Entry_result->id),
			),
		);
		$set_Prod_Quote_entry_result = call("set_entry", $set_Prod_Quote_entry_parameters, $url);
		echo "<br>Success: DrugItem ".$product_name." updated successfully !! ";
		}else{
			$set_Prod_Quote_entry_parameters = array(
				"session" => $session_id,
				"module_name" => "AOS_Products_Quotes",
				"name_value_list" => array(
					array('name' => 'name', 'value' => $product_name),
					array('name' => 'description', 'value' => $description),
					array('name' => 'assigned_user_id','value' => $assigned_user_id),
					array('name' => 'modified_user_id','value' => $modified_user_id),
					array('name' => 'created_by', 'value' => $CreatedBY),
					array('name' => 'deleted', 'value' => $deleted),
					array('name' => 'currency_id', 'value' => $currency_id),
					array('name' => 'part_number', 'value' => $prod_description),
					array('name' => 'item_description', 'value' => $Generic_Name),
					array('name' => 'number', 'value' => '1'),
					array('name' => 'product_qty', 'value' => $product_qty),
					array('name' => 'product_list_price', 'value' => $prodListPice),
					//array('name' => 'product_discount', 'value' => $product_discount),
					array('name' => 'product_discount', 'value' => $Savings),
					array('name' => 'vat', 'value' => $product_vat),
					array('name' => 'product_discount_amount', 'value' => $Savings),
					array('name' => 'discount', 'value' => $discount),
					array('name' => 'product_unit_price', 'value' => $product_unit_price),
					array('name' => 'product_total_price', 'value' => $product_total_price),
					array('name' => 'parent_type', 'value' => "AOS_Invoices"),
					array('name' => 'parent_id', 'value' => $set_entry_result->id),
					array('name' => 'product_id', 'value' => 'NULL'),
					array('name' => 'group_id', 'value' => $set_Line_Item_Entry_result->id),
				),
			);
			$set_Prod_Quote_entry_result = call("set_entry", $set_Prod_Quote_entry_parameters, $url);
			echo "<br>Success: DrugItem ".$product_name." created successfully !! ";
			}
			/* START ********* PRODUCT QUOTE CREATE / UPDATE *********/
		}else{
			Echo "<br>Failure: Product Name field is required, can't empty !!";
		}
	}
}else{
	Echo "<br>Failure: Order Is not available in CRM !!";
}	
}else{
	Echo "<br>Failure: Drug Item Array can't empty !!";
}

?>