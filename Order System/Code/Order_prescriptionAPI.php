<?php
   require_once('api_config.php');

    //function to make cURL request
    function call($method, $parameters, $url)
    {
        ob_start();
        $curl_request = curl_init();

        curl_setopt($curl_request, CURLOPT_URL, $url);
        curl_setopt($curl_request, CURLOPT_POST, 1);
        curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($curl_request, CURLOPT_HEADER, 1);
        curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

        $jsonEncodedData = json_encode($parameters);

        $post = array(
             "method" => $method,
             "input_type" => "JSON",
             "response_type" => "JSON",
             "rest_data" => $jsonEncodedData
        );

        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($curl_request);
        curl_close($curl_request);

        $result = explode("\r\n\r\n", $result, 2);
        $response = json_decode($result[1]);
        ob_end_flush();

        return $response;
    }

    //login -----------------------------------------     
	$login_parameters = array(
         "user_auth" => array(
              "user_name" => $username,
              "password" => md5($password),
              "version" => "1"
         ),
         "application_name" => "RestTest",
         "name_value_list" => array(),
    );

    $login_result = call("login", $login_parameters, $url);
    
    //get session id
    $session_id = $login_result->id;
	
	$data = json_decode(file_get_contents("php://input"), true);
	
	
	/*ORDER INPUTS*/
	/* $OrderID			=	'NMS160817892907N';
	$description		=	'Hi Update, This is only test Update1 Order by Mr. Nitin';
	$Status				=	'InTransit';
	$accountEmail		=	'dhanpal101@gmail.com'; */
	
	/*DOCUMENT INPUT*/
	/* $DOC_description	=	'Hi , This is only test1 Prescription by Mr. Nitin';
	$DOC_Status			= 	'Active';
	$DOC_filename 		= 	'Projects.pdf';
	$DOC_FILEPATH = "/var/www/html/SuiteCRM-7.6.5/Projects.pdf"; */
	
/* 	$ArrayLineItem['Line_items']['prod_description']= array(
						"product_qty" => 2,
						"Purchase_Price" => 100,
						"Drug_Code" => 'prod_description',
						"Generic_Name" => 'Generic_Name1',
						"Us_Brand_Name" => 'prod_name1',
						"product_discount" => '10.00',
						"cat_Discount" => '10.00',
						"coupon_Discount" => '10.00',
						"Prod_Shipping" => '10.00',
						"discount" => 'Percentage',
						"spl_Notes" => 'LineItem test for product ITEM-101-Test API',
						"Order_Id" => 'NMS160817892907N'
					);
	$ArrayLineItem['Line_items']['PRO-TEST 101']= array(
						"product_qty" => 2,
						"Purchase_Price" => 100,
						"Drug_Code" => 'PRO-TEST 101',
						"Generic_Name" => 'Generic_Name2',
						"Us_Brand_Name" => 'prod_name2',
						"product_discount" => '10.00',
						"cat_Discount" => '10.00',
						"coupon_Discount" => '10.00',
						"Prod_Shipping" => '10.00',	
						"discount" => 'Percentage',
						"spl_Notes" => 'LineItem test for product PRO-TEST 101 API',
						"Order_Id" => 'NMS160817892907N'

					);
	$data = array(
			   "order" => array(
					"Status" => $Status,
					"accountEmail" => $accountEmail,
					"description" => $description,
					"OrderID" => $OrderID
				),
			  "prescription" => array(
					"DOC_description" => $DOC_description,
					"DOC_filename" => $DOC_filename,
					"DOC_Status" => $DOC_Status,
					"DOC_FILEPATH" => $DOC_FILEPATH
				),

			  "invoice" => array(					
					"Line_items" => $ArrayLineItem['Line_items']
				),
			);  */  
$OrderID			=	$data['order']['OrderID'];
$accountEmail		=	$data['order']['email_id'];
$assigned_user_id	=	'1';
$CreatedBY			=	'1';
$modified_user_id	=	'1';
$deleted			=	'0';
$Status				=	$data['order']['Status'];
//$accountEmail		=	$data['order']['accountEmail'];

/*Documents FIELDS*/
/* $docCount  = count($data['prescription_list']['prescription']);
if($docCount > 0){
	foreach($data['prescription_list']['prescription'] as $prescription => $DocDetail){
		$DOC_description	=	$data['prescription']['DOC_description'];
		$DOC_Status			= 	$data['prescription']['DOC_Status'];
		$filename 			= 	$data['prescription']['DOC_filename'];
		$contents = file_get_contents ($data['prescription']['DOC_FILEPATH']);
		//$contents = file_get_contents ("/var/www/html/SuiteCRM-7.6.5/Projects.pdf");
		$mimeType = mime_content_type($filename); 
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
	}	
} */

$AccountID='';
$OID='';
if($accountEmail!=''){
	$email_UPC=strtoupper($accountEmail);
	//GETTING email_addr_bean_rel  Entry
	
	$get_entry_list_parameters = array(
		 'session' => $session_id,
		 'module_name' => 'Accounts',
		 'query' => " accounts.id IN (SELECT bean_id FROM email_addr_bean_rel eabr JOIN email_addresses ea ON (eabr.email_address_id = ea.id) WHERE bean_module = 'Accounts' AND ea.email_address_caps LIKE '%".$email_UPC."' AND eabr.deleted=0) ",
		
		 'order_by' => "",
		 'offset' => '0',
		 'select_fields' => array(
			  'id',
			  'name',
		 ),
		 'link_name_to_fields_array' => array(array('name' => 'email_addresses', 'value' => array('id', 'email_address', 'opt_out', 'primary_address'))),
		 'max_results' => '',
		 'deleted' => '0',
		 'Favorites' => false,
	);
	$datalead_bean_Email = call ("get_entry_list", $get_entry_list_parameters, $url );
	if(count($datalead_bean_Email->entry_list)>0)
		$AccountID=$datalead_bean_Email->entry_list[0]->id;
}else{
	echo "<br>Failure: Account Email id can't Empty";
}

//Validating Order if exist than only updating
if($OrderID!=''){
	$get_entry_list_parameters = array(
		 'session' => $session_id,
		 'module_name' => 'nm_Order',
		 'query' => " nm_order.name='".$OrderID."'",
		 'order_by' => "",
		 'offset' => '0',
		 'select_fields' => array(
			  'id',
			  'name',
		 ),
		 'link_name_to_fields_array' => array(),
		 'max_results' => '',
		 'deleted' => '0',
		 'Favorites' => false,
	);
	$get_entry_list_result = call('get_entry_list', $get_entry_list_parameters, $url);
	if(count($get_entry_list_result->entry_list)>0)
		$OID=$get_entry_list_result->entry_list[0]->id;
}else{
	echo "<br>Failure: Order ID can't Empty";
}
#START :: CREATE OR UPDATE ORDER
if($OID != '' && $accountEmail!=''){
	
	#START :: CHECK IF DOCUMENT EXISTING WITH CURRENT ORDER
	$docCount  = count($data['prescription_list']);
	$docOrder = $OID;
	
	/*START :: DELETE :: BEFORE ADD/UPDATE DOCUMENT,DELETE ALL PREVIOUS DOCUMENT */
	$get_entry_doc_parameters = array(
		 'session' => $session_id,
		 'module_name' => 'Documents',
		 
		 'query' => " documents.order_id = '".$docOrder."' ",
		 'order_by' => "",
		 'offset' => '0',
		 'select_fields' => array(
			  'id',
			  'document_name',
		 ),
		 'link_name_to_fields_array' => array(),
		 //'link_name_to_fields_array' => array(array('name' => 'nm_order_documents_c', 'value' => array('nm_order_documentsdocuments_idb'))),
		 'max_results' => '',
		 'deleted' => '0',
		 'Favorites' => false,
	);
	
	$get_entry_doc_result = call('get_entry_list', $get_entry_doc_parameters, $url);

	$PreviousDocCount = count($get_entry_doc_result->entry_list);
	if($PreviousDocCount > 0){
		for($i = 0; $i < $PreviousDocCount; $i++){
			$DocumentID = $get_entry_doc_result->entry_list[$i]->name_value_list->id->value;
			if($DocumentID != ''){
			
					# DELETE :: DOCUMENT 
				$set_entry_parameters = array(
					"session" => $session_id,
					"module_name" => "Documents",
					"name_value_list" => array(
						array("name" => "id", "value" => $DocumentID),
						array( 'name' => 'deleted', 'value' => '1'),
					),
				);

				$set_entryDOC_result = call("set_entry", $set_entry_parameters, $url);
				# DELETE RELATION :: DOCUMENT-ORDER 
				$set_order_doc_relationship_parameters = array(
					'session' => $session_id,
					'module_name' => 'Documents',
					'module_id' => $DocumentID,
					'link_field_name' => 'nm_order_documentsnm_order_ida',
					'related_ids' => array(
						$docOrder
					),
					'name_value_list' => array(
						
					),
					'delete'=> 1,
					
				);

				$set_Order_DOC_relationship_result = call("set_relationship", $set_order_doc_relationship_parameters, $url);
				
				# DELETE RELATION :: DOCUMENT-ACCOUNT 
				$set_Account_relationship_parameters = array(
					'session' => $session_id,
					'module_name' => 'Documents',
					'module_id' => $DocumentID,
					'link_field_name' => 'accounts',
					'related_ids' => array(
						$AccountID
					),
					'name_value_list' => array(
						
					),
					'delete'=> 1,
				);

				$set_Account_relationship_result = call("set_relationship", $set_Account_relationship_parameters, $url);
			}
		}
	}
	/*END :: DELETE :: BEFORE ADD/UPDATE DOCUMENT,DELETE ALL PREVIOUS DOCUMENT */
	
	/*START :: INSERT :: NEW DOCUMENT */
	
	if($docCount > 0){
		foreach($data['prescription_list'] as $prescription => $DocDetail){
			$DocumentID = '';
			$DOC_description	=	$data['prescription_list'][$prescription]['DOC_description'];
			$DOC_Status			= 	$data['prescription_list'][$prescription]['DOC_Status'];
			//$DOC_Remove			= 	$data['prescription_list'][$prescription]['DOC_Remove'];
			$DOC_updated_on			= 	$data['prescription_list'][$prescription]['DOC_updated_on'];
			$DOC_updated_by			= 	$data['prescription_list'][$prescription]['DOC_updated_by'];
			$filename 			= 	$data['prescription_list'][$prescription]['DOC_filename'];
			if($data['prescription_list'][$prescription]['DOC_FILEPATH'] != ''){
				$opts = array('http' =>
					array(
						'method'  => 'GET',
						'timeout' => 3 
					)
				);
				$context  = stream_context_create($opts);
				$contents = file_get_contents ($data['prescription_list'][$prescription]['DOC_FILEPATH'],false,$context);
			}
			//$contents = file_get_contents ("/var/www/html/SuiteCRM-7.6.5/Projects.pdf");
			$mimeType = mime_content_type($filename); 
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			
				
			if($DocumentID ==''){

				if($filename != ''  && ($contents != '' || $contents != false)){
					//create document -------------------------------------------- 
					$set_entry_parameters = array(
						"session" => $session_id,
						"module_name" => "Documents",
						"name_value_list" => array(
							array("name" => "document_name", "value" => $filename),
							array("name" => "revision", "value" => "1"),
							array("name" => "description", "value" => $DOC_description),
							array("name" => "status_id", "value" => 'Active'),
							array("name" => "updated_by", "value" => $DOC_updated_by),
							array("name" => "updated_on", "value" => $DOC_updated_on),
							array("name" => "doc_type", "value" => "Sugar"),
							array( 'name' => 'assigned_user_id','value' => $assigned_user_id),
							array( 'name' => 'modified_user_id','value' => $modified_user_id),
							array( 'name' => 'created_by', 'value' => $CreatedBY),
							array( 'name' => 'order_status', 'value' => $DOC_Status),
							array( 'name' => 'deleted', 'value' => $deleted),
							array( 'name' => 'order_id', 'value' => $docOrder),
						),
					);

					$set_entryDOC_result = call("set_entry", $set_entry_parameters, $url);

					# RELATION :: DOCUMENT-ACCOUNT 
					$set_Account_relationship_parameters = array(
						'session' => $session_id,
						'module_name' => 'Documents',
						'module_id' => $set_entryDOC_result->id,
						'link_field_name' => 'accounts',
						'related_ids' => array(
							$AccountID
						),
						'name_value_list' => array(
							
						),
						'delete'=> 0,
					);

					$set_Account_relationship_result = call("set_relationship", $set_Account_relationship_parameters, $url);
					
					# RELATION :: ORDER-PRESCRIPTION 
					if($docOrder != ''){
						$set_order_doc_relationship_parameters = array(
							'session' => $session_id,
							'module_name' => 'Documents',
							'module_id' => $set_entryDOC_result->id,
							'link_field_name' => 'nm_order_documentsnm_order_ida',
							'related_ids' => array(
								$docOrder
							),
							'name_value_list' => array(
								
							),
							'delete'=> 0,
							
						);

						$set_Order_DOC_relationship_result = call("set_relationship", $set_order_doc_relationship_parameters, $url);
					}
					
					#create document revision ------------------------------------ 
					$set_document_revision_parameters = array(
						"session" => $session_id,
						"note" => array(
							'id' => $set_entryDOC_result->id,
							'file' => base64_encode($contents),
							'filename' => $filename,
							'change_log' => 'Document Created',
							'doc_type' => 'Sugar',
							'file_mime_type' => $mimeType,
							'file_ext' => $ext,
							'revision' => '1',
						),
					);

					$set_document_revision_result = call("set_document_revision", $set_document_revision_parameters, $url);
					echo "<br>Success: ".$filename." Document uploaded successfully";
				}else{
					//echo "<br>Failure: FileName & File Path can't empty";
					echo "<br>Failure: Rx not sent";
				}
				
			}else{

					echo "<br>Failure: ".$filename." Document allready exist";
				
			}
		}	
	}else{
		echo "<br>Failure: Rx not sent";
	}
	/*END :: INSERT :: NEW DOCUMENT */
	
}

?>