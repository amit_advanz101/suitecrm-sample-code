<?php
   require_once('api_config.php');

    //function to make cURL request
    function call($method, $parameters, $url)
    {
        ob_start();
        $curl_request = curl_init();

        curl_setopt($curl_request, CURLOPT_URL, $url);
        curl_setopt($curl_request, CURLOPT_POST, 1);
        curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($curl_request, CURLOPT_HEADER, 1);
        curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

        $jsonEncodedData = json_encode($parameters);

        $post = array(
             "method" => $method,
             "input_type" => "JSON",
             "response_type" => "JSON",
             "rest_data" => $jsonEncodedData
        );

        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($curl_request);
        curl_close($curl_request);

        $result = explode("\r\n\r\n", $result, 2);
        $response = json_decode($result[1]);
        ob_end_flush();

        return $response;
    }

    //login -----------------------------------------     
	$login_parameters = array(
         "user_auth" => array(
              "user_name" => $username,
              "password" => md5($password),
              "version" => "1"
         ),
         "application_name" => "RestTest",
         "name_value_list" => array(),
    );

    $login_result = call("login", $login_parameters, $url);
    
    //get session id
    $session_id = $login_result->id;
	
	$data = json_decode(file_get_contents("php://input"), true);
	
	
	/*ORDER INPUTS*/
	/* $OrderID			=	'NMS160817892907N';
	$description		=	'Hi Update, This is only test Update1 Order by Mr. Nitin';
	$Status				=	'InTransit';
	$accountEmail		=	'dhanpal101@gmail.com'; */
	
	/*DOCUMENT INPUT*/
	/* $DOC_description	=	'Hi , This is only test1 Prescription by Mr. Nitin';
	$DOC_Status			= 	'Active';
	$DOC_filename 		= 	'Projects.pdf';
	$DOC_FILEPATH = "/var/www/html/SuiteCRM-7.6.5/Projects.pdf"; */
	
/* 	$ArrayLineItem['Line_items']['prod_description']= array(
						"product_qty" => 2,
						"Purchase_Price" => 100,
						"Drug_Code" => 'prod_description',
						"Generic_Name" => 'Generic_Name1',
						"Us_Brand_Name" => 'prod_name1',
						"product_discount" => '10.00',
						"cat_Discount" => '10.00',
						"coupon_Discount" => '10.00',
						"Prod_Shipping" => '10.00',
						"discount" => 'Percentage',
						"spl_Notes" => 'LineItem test for product ITEM-101-Test API',
						"Order_Id" => 'NMS160817892907N'
					);
	$ArrayLineItem['Line_items']['PRO-TEST 101']= array(
						"product_qty" => 2,
						"Purchase_Price" => 100,
						"Drug_Code" => 'PRO-TEST 101',
						"Generic_Name" => 'Generic_Name2',
						"Us_Brand_Name" => 'prod_name2',
						"product_discount" => '10.00',
						"cat_Discount" => '10.00',
						"coupon_Discount" => '10.00',
						"Prod_Shipping" => '10.00',	
						"discount" => 'Percentage',
						"spl_Notes" => 'LineItem test for product PRO-TEST 101 API',
						"Order_Id" => 'NMS160817892907N'

					);
	$data = array(
			   "order" => array(
					"Status" => $Status,
					"accountEmail" => $accountEmail,
					"description" => $description,
					"OrderID" => $OrderID
				),
			  "prescription" => array(
					"DOC_description" => $DOC_description,
					"DOC_filename" => $DOC_filename,
					"DOC_Status" => $DOC_Status,
					"DOC_FILEPATH" => $DOC_FILEPATH
				),

			  "invoice" => array(					
					"Line_items" => $ArrayLineItem['Line_items']
				),
			);  */  
$OrderID			=	$data['order']['OrderID'];
$description		=	$data['order']['description'];
$assigned_user_id	=	'1';
$CreatedBY			=	'1';
$modified_user_id	=	'1';
$deleted			=	'0';
$Status				=	$data['order']['Status'];
$order_date			=	$data['order']['order_date'];
$accountEmail		=	$data['order']['email_id'];
$cod_charge			=	$data['order']['cod_charge'];
$customer_id		=	$data['order']['customer_id'];
$discount_amt		=	$data['order']['discount_amt'];
$doctor_name		=	$data['order']['doctor_name'];
$order_type			=	$data['order']['order_type'];
$payment_mode		=	$data['order']['payment_mode'];
$person_name		=	$data['order']['person_name'];
$prod_shipping		=	$data['order']['prod_shipping'];
$product_amt		=	$data['order']['product_amt'];
$ship_location		=	$data['order']['ship_location'];
$spl_notes			=	$data['order']['spl_notes'];
$total_amt			=	$data['order']['total_amt'];
$tracking_number	=	$data['order']['tracking_number'];
$used_wallet_amt	=	$data['order']['used_wallet_amt'];
$voucher_amt		=	$data['order']['voucher_amt'];
//Added address field :: 20 oct 2016
$Billing_Name		=	$data['order']['Billing_Name'];
$Billing_Phone_No	=	$data['order']['Billing_Phone_No'];
$Billing_Address1	=	$data['order']['Billing_Address'];
$Billing_City		=	$data['order']['Billing_City'];
$Billing_Zip_Code	=	$data['order']['Billing_Zip_Code'];
$Billing_State		=	$data['order']['Billing_State'];
$Billing_Country	=	$data['order']['Billing_Country'];
$Shipping_Address1	=	$data['order']['Shipping_Address'];
$Shipping_City		=	$data['order']['Shipping_City'];
$Shipping_State		=	$data['order']['Shipping_State'];
$Shipping_Zip_Code	=	$data['order']['Shipping_Zip_Code'];
$Shipping_Country	=	$data['order']['Shipping_Country'];
$Shipping_Name		=	$data['order']['Shipping_Name'];
$Shipping_Phone_No	=	$data['order']['Shipping_Phone_No'];



//INVOICE MODULE FIELDS	
$Order_ID='';
$AccountID='';

$OID='';
if($accountEmail!=''){
	$email_UPC=strtoupper($accountEmail);
	//GETTING email_addr_bean_rel  Entry
	
	$get_entry_list_parameters = array(
		 'session' => $session_id,
		 'module_name' => 'Accounts',
		 'query' => " accounts.id IN (SELECT bean_id FROM email_addr_bean_rel eabr JOIN email_addresses ea ON (eabr.email_address_id = ea.id) WHERE bean_module = 'Accounts' AND ea.email_address_caps LIKE '%".$email_UPC."' AND eabr.deleted=0) ",
		
		 'order_by' => "",
		 'offset' => '0',
		 'select_fields' => array(
			  'id',
			  'name',
		 ),
		 'link_name_to_fields_array' => array(array('name' => 'email_addresses', 'value' => array('id', 'email_address', 'opt_out', 'primary_address'))),
		 'max_results' => '',
		 'deleted' => '0',
		 'Favorites' => false,
	);
	$datalead_bean_Email = call ("get_entry_list", $get_entry_list_parameters, $url );
	if(count($datalead_bean_Email->entry_list)>0)
		$AccountID=$datalead_bean_Email->entry_list[0]->id;
}else{
	echo "<br>Failure: Account Email id can't Empty";
}

//Validating Order if exist than only updating
if($OrderID!=''){
	$get_entry_list_parameters = array(
		 'session' => $session_id,
		 'module_name' => 'nm_Order',
		 'query' => " nm_order.name='".$OrderID."'",
		 'order_by' => "",
		 'offset' => '0',
		 'select_fields' => array(
			  'id',
			  'name',
		 ),
		 'link_name_to_fields_array' => array(),
		 'max_results' => '',
		 'deleted' => '0',
		 'Favorites' => false,
	);
	$get_entry_list_result = call('get_entry_list', $get_entry_list_parameters, $url);
	if(count($get_entry_list_result->entry_list)>0)
		$OID=$get_entry_list_result->entry_list[0]->id;
}else{
	echo "<br>Failure: Order ID can't Empty";
}
#START :: CREATE OR UPDATE ORDER
if($OrderID != '' && $accountEmail!=''){
	if($OID != ''){
		$set_entry_parameters = array(
			"session" => $session_id,
			"module_name" => "nm_Order",
			"name_value_list" => array(
				array( 'name' => 'id', 'value' => $OID),
				array( 'name' => 'name', 'value' => $OrderID),
				array( 'name' => 'description', 'value' => $description),
				array( 'name' => 'assigned_user_id','value' => $assigned_user_id),
				array( 'name' => 'modified_user_id','value' => $modified_user_id),
				array( 'name' => 'created_by', 'value' => $CreatedBY),
				array( 'name' => 'deleted', 'value' => $deleted),
				array( 'name' => 'status', 'value' => $Status),
				array( 'name' => 'order_date', 'value' => $order_date),
				array( 'name' => 'account_id_c', 'value' => $AccountID),
				array( 'name' => 'payment_mode', 'value' => $payment_mode),
				array( 'name' => 'person_name', 'value' => $person_name),
				array( 'name' => 'product_amt', 'value' => $product_amt),
				array( 'name' => 'prod_shipping', 'value' => $prod_shipping),
				array( 'name' => 'ship_location', 'value' => $ship_location),
				array( 'name' => 'spl_notes', 'value' => $spl_notes),
				array( 'name' => 'total_amt', 'value' => $total_amt),
				array( 'name' => 'tracking_number', 'value' => $tracking_number),
				array( 'name' => 'used_wallet_amt', 'value' => $used_wallet_amt),
				array( 'name' => 'voucher_amt', 'value' => $voucher_amt),
				array( 'name' => 'email_id', 'value' => $accountEmail),
				array( 'name' => 'cod_charge', 'value' => $cod_charge),
				array( 'name' => 'customer_id', 'value' => $customer_id),
				array( 'name' => 'discount_amt', 'value' => $discount_amt),
				array( 'name' => 'doctor_name', 'value' => $doctor_name),
				array( 'name' => 'order_type', 'value' => $order_type),
				array( 'name' => 'billing_name', 'value' => $Billing_Name),
				array( 'name' => 'billing_phone_no', 'value' => $Billing_Phone_No),
				array( 'name' => 'billing_address_street', 'value' => $Billing_Address1),
				array( 'name' => 'billing_address_city', 'value' => $Billing_City),
				array( 'name' => 'billing_address_postalcode', 'value' => $Billing_Zip_Code),
				array( 'name' => 'billing_address_state', 'value' => $Billing_State),
				array( 'name' => 'billing_address_country', 'value' => $Billing_Country),
				array( 'name' => 'shipping_address_street', 'value' => $Shipping_Address1),
				array( 'name' => 'shipping_address_city', 'value' => $Shipping_City),
				array( 'name' => 'shipping_address_state', 'value' => $Shipping_State),
				array( 'name' => 'shipping_address_postalcode', 'value' => $Shipping_Zip_Code),
				array( 'name' => 'shipping_address_country', 'value' => $Shipping_Country),
				array( 'name' => 'shipping_name', 'value' => $Shipping_Name),
				array( 'name' => 'shipping_phone_no', 'value' => $Shipping_Phone_No),
			),
		);
		$set_entry_result = call("set_entry", $set_entry_parameters, $url);
		echo "<br>Success: Order updated successfully!!";
		
	}else{
		$set_entry_parameters = array(
			"session" => $session_id,
			"module_name" => "nm_Order",
			"name_value_list" => array(
				array( 'name' => 'name', 'value' => $OrderID),
				array( 'name' => 'description', 'value' => $description),
				array( 'name' => 'assigned_user_id','value' => $assigned_user_id),
				array( 'name' => 'modified_user_id','value' => $modified_user_id),
				array( 'name' => 'created_by', 'value' => $CreatedBY),
				array( 'name' => 'deleted', 'value' => $deleted),
				array( 'name' => 'status', 'value' => $Status),
				array( 'name' => 'order_date', 'value' => $order_date),
				array( 'name' => 'account_id_c', 'value' => $AccountID),
				array( 'name' => 'payment_mode', 'value' => $payment_mode),
				array( 'name' => 'person_name', 'value' => $person_name),
				array( 'name' => 'product_amt', 'value' => $product_amt),
				array( 'name' => 'prod_shipping', 'value' => $prod_shipping),
				array( 'name' => 'ship_location', 'value' => $ship_location),
				array( 'name' => 'spl_notes', 'value' => $spl_notes),
				array( 'name' => 'total_amt', 'value' => $total_amt),
				array( 'name' => 'tracking_number', 'value' => $tracking_number),
				array( 'name' => 'used_wallet_amt', 'value' => $used_wallet_amt),
				array( 'name' => 'voucher_amt', 'value' => $voucher_amt),
				array( 'name' => 'email_id', 'value' => $accountEmail),
				array( 'name' => 'cod_charge', 'value' => $cod_charge),
				array( 'name' => 'customer_id', 'value' => $customer_id),
				array( 'name' => 'discount_amt', 'value' => $discount_amt),
				array( 'name' => 'doctor_name', 'value' => $doctor_name),
				array( 'name' => 'order_type', 'value' => $order_type),
				array( 'name' => 'billing_name', 'value' => $Billing_Name),
				array( 'name' => 'billing_phone_no', 'value' => $Billing_Phone_No),
				array( 'name' => 'billing_address_street', 'value' => $Billing_Address1),
				array( 'name' => 'billing_address_city', 'value' => $Billing_City),
				array( 'name' => 'billing_address_postalcode', 'value' => $Billing_Zip_Code),
				array( 'name' => 'billing_address_state', 'value' => $Billing_State),
				array( 'name' => 'billing_address_country', 'value' => $Billing_Country),
				array( 'name' => 'shipping_address_street', 'value' => $Shipping_Address1),
				array( 'name' => 'shipping_address_city', 'value' => $Shipping_City),
				array( 'name' => 'shipping_address_state', 'value' => $Shipping_State),
				array( 'name' => 'shipping_address_postalcode', 'value' => $Shipping_Zip_Code),
				array( 'name' => 'shipping_address_country', 'value' => $Shipping_Country),
				array( 'name' => 'shipping_name', 'value' => $Shipping_Name),
				array( 'name' => 'shipping_phone_no', 'value' => $Shipping_Phone_No),
			),
		);
		$set_entry_result = call("set_entry", $set_entry_parameters, $url);		
		echo "<br>Success: Order created successfully!!";
		

	}		
	#END :: CREATE OR UPDATE ORDER
	
	
	
	#START :: CREATE RELATION ACCOUNT - ORDER
	if($AccountID!=''){
		$set_relationship_parameters = array(
			'session' => $session_id,
			'module_name' => 'nm_Order',
			'module_id' => $set_entry_result->id,
			'link_field_name' => 'nm_order_accountsaccounts_ida',
			'related_ids' => array(
				$AccountID,
			),
			'name_value_list' => array(
			),
			'delete'=> 0,
		);

		$set_relationship_result = call("set_relationship", $set_relationship_parameters, $url);
	} 
	#END :: CREATE RELATION ACCOUNT - ORDER
}

?>